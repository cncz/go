SHELL := /bin/bash
PKG   := $(dir $(wildcard */.))
MAN   := $(wildcard cmd/*/*.[0-9])
# put FITLER var here and factor it out below
DIR   := hugo/content/pkg

.PHONY: doc
doc:
	mkdir -p $(DIR)
	for pkg in $(PKG); do \
		if test $$pkg = "public/" ; then continue; fi ;\
		if test $$pkg = "cmd/" ; then continue; fi ;\
		if test $$pkg = "hugo/" ; then continue; fi ;\
		cmd/doc2hugo/doc2hugo -import "go.science.ru.nl/"$$pkg -o $(DIR) $$pkg ;\
		mkdir public/$$pkg ;\
		cp public/index.html public/$$pkg/index.html ;\
	done
	mkdir public/cmd
	cp public/index.html public/cmd/index.html

.PHONY: hugo
hugo:
	( cd hugo; CI_COMMIT_SHA=$$(git rev-parse HEAD) hugo -d ../public/pkg )

.PHONY: test
test:
	for pkg in $(PKG); do \
		if test $$pkg = "public/" ; then continue; fi ;\
		if test $$pkg = "hugo/" ; then continue; fi ;\
		( cd $$pkg; go test ./...) ;\
	done

.PHONY: man
man:
	if ! command -v mmark; then echo "mmark needed for manpage generation"; fi ;\
	for man in $(MAN); do \
		mmark -man $$man.md > $$man ; \
	done

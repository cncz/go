# C&CZ Main Go Repository

Packages gaan gewoon in de top-level directory. Er zijn een paar directories voor "support":

* `public`: een index.html om Go package imports mogelijk te maken voor go.science.ru.nl
* `hugo`: hugo site om de Go package te genereren, wordt in `public/pkg` gezet.
    Dit geheel werkt samen met `cmd/doc2hugo` om alles te publiceren.
* `cmd`: plek for Go CLIs.

Alle andere top level directories zijn een Go package zijn. Hou een package redelijk schoon, dat
betekent:

* Geen binaries erin (binaries die handig zijn kunnen in `cmd`).
* Goede Go documentatie. Documentatie kan via de "godoc" server geraadpleegd worden:
  <https://go.science.ru.nl/pkg>
* Link de LICENCE file in je nieuwe repo, zodat onze packages ook opgepikt kunnen worden door
  "pkg.go.dev"

De `public/index.html` is belangrijk want die vertelt `go get` waar `go.science.ru.nl` packages te
vinden zijn tijdens het compileren.

Via een Gitlab pipeline wordt het allemaal aan elkaar geknoopt:

* de Go code wordt getest
* de documentatie wordt eruit gehaald en in hugo/content/pkg neer gezet
* hugo compileert de 'site' naar public/pkg

Eind resultaat:

`go.science.ru.nl` is het *import path* voor onze Go code. And op <https://go.science.ru.nl/pkg>
kan alle Go documentatie worden gelezen.

package promfmt

import (
	"bytes"
	"io/ioutil"
	"os"
	"path"

	"github.com/prometheus/client_golang/prometheus"
)

// WriteFile will gather all metrics using the prometheus.DefaultGatherer and will transform the metrics into text and
// write to the file named name. The file write is atomic (on Unix) by writing to a temp file and then moving it into
// place.
func WriteFile(name string, f ...FilterFunc) error {
	mfs, err := prometheus.DefaultGatherer.Gather()
	if err != nil {
		return err
	}
	buf := &bytes.Buffer{}
	if _, err := text(mfs, buf, f...); err != nil {
		return err
	}

	temp, err := ioutil.TempFile(path.Dir(name), "*")
	if err != nil {
		return err
	}
	defer os.Remove(temp.Name())

	err = os.WriteFile(temp.Name(), buf.Bytes(), 0644)
	if err != nil {
		return err
	}
	// force 0644

	if err := os.Chmod(temp.Name(), 0644); err != nil {
		return err
	}

	return os.Rename(temp.Name(), name)
}

package promfmt

import (
	"bytes"
	"os"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
	Name: "promtest_processed_ops_total",
	Help: "The total number of processed events",
})

func TestText(t *testing.T) {
	opsProcessed.Inc()
	tf := NewPrefixFilter("promtest_")

	b := &bytes.Buffer{}
	exp := `# HELP promtest_processed_ops_total The total number of processed events
# TYPE promtest_processed_ops_total counter
promtest_processed_ops_total 1
`
	Fprint(b, tf)
	if b.String() != exp {
		t.Fatalf("expected %s, got %s", b, exp)
	}
}

func TestWriteFile(t *testing.T) {
	opsProcessed.Inc()
	err := WriteFile("testout", NewPrefixFilter("promtest_"))
	if err != nil {
		t.Fatal(err)
	}

	defer os.Remove("testout")
	_, err = os.ReadFile("testout")
	if err != nil {
		t.Fatal(err)
	}
	// some fancy compare on the data in the file?
}

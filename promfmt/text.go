// Package promfmt exports prometheus metrics to the default text format.
//
// Basic usage to only show metrics that start with "promtest_"
//
//	var opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
//		Name: "promtest_processed_ops_total",
//		Help: "The total number of processed events",
//	})
//
//	func main() {
//		opsProcessed.Inc()
//		tf := promfmt.NewPrefixFilter("promtest_")
//		promfmt.Fprint(os.Stdout, tf)
//	}
//
// Main usage is aimed at simple Go binaries that run via CRON and dump their metrics in node exporter so it can be
// picked up. This negates the need to run your own webserver to expose the metrics and the assorted discovery from
// prometheus.
package promfmt

import (
	"io"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	dto "github.com/prometheus/client_model/go"
	"github.com/prometheus/common/expfmt"
)

// FilterFunc is used to filter metrics, when the returned boolean is false the metrics is excluded.
type FilterFunc func(mf *dto.MetricFamily) bool

// Fprint will gather all metrics using the prometheus.DefaultGatherer and will transform the metrics into text and
// write the result in w. The optional FilterFunc is used to filter out any unwanted metrics.
func Fprint(w io.Writer, f ...FilterFunc) (int, error) {
	mfs, err := prometheus.DefaultGatherer.Gather()
	if err != nil {
		return 0, err
	}
	return text(mfs, w, f...)
}

// text transforms the metrics in mfs to text and writes the result into w.
// It returns the number of bytes written and any error encountered.
// The optional FilterFunc is used to filter out any unwanted metrics.
func text(mfs []*dto.MetricFamily, w io.Writer, f ...FilterFunc) (n int, err error) {
	for _, mf := range mfs {
		ok := true
		if len(f) > 0 {
			ok = f[0](mf)
		}
		if ok {
			n1, err1 := expfmt.MetricFamilyToText(w, mf)
			n += n1
			if err1 != nil {
				return n, err1
			}
		}
	}
	return n, err
}

// NewPrefixFilter returns a FilterFunc that can be used to filter on the prefix of the metric name.
func NewPrefixFilter(prefix string) func(*dto.MetricFamily) bool {
	return func(mf *dto.MetricFamily) bool {
		return strings.HasPrefix(*mf.Name, prefix)
	}
}

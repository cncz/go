locksne

Go program that exports sytems locks data to prometheus node exporter.

The following metrics are dumped:

- `locksne_last_run_timestamp_seconds <epoch>` (gauge)
- `locksne_run_duration_seconds <duration>` (gauge)
- `locksne_lock_count_total{share="<export/share>",type="<type">} <num>` (gauge)
    ~~~
    COMMAND             PID  TYPE  SIZE MODE  M      START        END PATH
    atd                2338 POSIX       WRITE 0          0          0 /run...
    containerd         2536 FLOCK       WRITE 0          0          0 /var/lib...
    dockerd            4031 FLOCK       WRITE 0          0          0 /var/lib/docker...
    dockerd            4031 FLOCK       WRITE 0          0          0 /var/lib/docker...
    ~~~
    We parse the json output of lslocks. Only nfsd and smbd commands are exported.

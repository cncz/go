package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	metricLastRunTimestamp = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "locksne_last_run_time_seconds",
		Help: "Epoch timestamp of the last run.",
	})
	metricRunDuration = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "locksne_run_duration_seconds",
		Help: "Gauge of current configured loop time.",
	})
	metricLockTotal = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "locksne_lock_count",
		Help: "Gauge of total locked files in a share.",
	}, []string{"share", "command"})
)

package main

import (
	"encoding/json"
	"os/exec"
	"strings"
)

// map of command,share -> #locks
type lock map[string]int

func lsLocks() ([]byte, error) {
	cmd := exec.Command("lslocks", "-J")
	out, err := cmd.Output()
	if err != nil {
		return out, err
	}
	return out, nil
}

type J struct {
	Command string `json:"command"`
	Share   string `json:"path"`
}

type L struct {
	Locks []J `json:"locks"`
}

func lsLocksLock(data []byte) (lock, error) {
	ls := lock{}
	j := L{}
	err := json.Unmarshal(data, &j)
	if err != nil {
		return nil, err
	}
	for _, v := range j.Locks {
		if v.Command != "nfsd" && v.Command != "smbd" {
			continue
		}
		if !strings.HasPrefix(v.Share, "/export") {
			continue
		}
		v.Share = strings.Replace(v.Share, "...", "", 1)

		paths := strings.Split(v.Share, "/")
		if len(paths) > 3 {
			v.Share = strings.Join(paths[:3], "/")
		}
		ls[v.Command+","+v.Share] += 1
	}

	return ls, nil
}

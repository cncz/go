package main

import (
	"testing"
)

const locklist = `{
   "locks": [
      {
         "command": "smbd",
         "pid": 2338,
         "type": "POSIX",
         "size": null,
         "mode": "WRITE",
         "m": false,
         "start": 0,
         "end": 0,
         "path": "/export/run"
      },{
         "command": "nfsd",
         "pid": 2536,
         "type": "FLOCK",
         "size": null,
         "mode": "WRITE",
         "m": false,
         "start": 0,
         "end": 0,
         "path": "/export/var/lib..."
      },{
         "command": "nfsd",
         "pid": 2536,
         "type": "FLOCK",
         "size": null,
         "mode": "WRITE",
         "m": false,
         "start": 0,
         "end": 0,
         "path": "/export..."
      }
   ]
}
`

func TestParseLock(t *testing.T) {
	locks, err := lsLocksLock([]byte(locklist))
	if err != nil {
		t.Errorf("%s", err)
	}
	if len(locks) != 3 {
		t.Errorf("expected 3 locks, got %d", len(locks))
	}
	v, ok := locks["nfsd,/export/var"]
	if !ok {
		t.Errorf("expected value for %s", "nfds,/export/var")
	}
	if v != 1 {
		t.Errorf("expected value %d, got %d", 1, v)
	}
}

package main

import "testing"

func TestRecord(t *testing.T) {
	rl, err := records("testdata/addresses.csv")
	if err != nil {
		t.Fatal(err)
	}
	if rl != 36.0 {
		t.Errorf("Expected %f, got %f", 36.0, rl)
	}
}

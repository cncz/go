package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	metricLastRunTimestamp = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "csvne_last_run_time_seconds",
		Help: "Epoch timestamp of the last run.",
	})
	metricRunDuration = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "csvne_run_duration_seconds",
		Help: "Gauge of current configured loop time.",
	})
	metricFileSize = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "csvne_file_size_bytes",
		Help: "Gauge of the file size in bytes.",
	}, []string{"file"})
	metricRecordCount = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "csvne_record_count_total",
		Help: "Gauge of the numbers of records in the file.",
	}, []string{"file"})
	metricFileMode = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "csvne_file_mode",
		Help: "Gauge of the file's mode. Stored in mode label.",
	}, []string{"file", "mode"})
)

package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"time"

	"go.science.ru.nl/log"
	"go.science.ru.nl/promfmt"
)

var (
	flagWrite    = flag.Bool("w", true, "write to /var/lib/prometheus/node-exporter/csvne.prom")
	flagDuration = flag.Uint("t", 60, "default duration to export in seconds")
	flagDebug    = flag.Bool("d", false, "enable debug logging")
)

const promfile = "/var/lib/prometheus/node-exporter/csvne.prom"

func main() {
	flag.Parse()
	if *flagDebug {
		log.D.Set()
	}
	if flag.NArg() == 0 {
		log.Fatal("Expected file name argument")
	}
	name := flag.Arg(0)

	fi, err := os.Stat(name)
	if err != nil {
		log.Fatalf("Failed to stat file %q: %s", name, err)
	}
	mode := fi.Mode()
	metricFileSize.WithLabelValues(name).Set(float64(fi.Size()))
	metricFileMode.WithLabelValues(name, mode.String()).Set(1)

	rl, err := records(name)
	if err != nil {
		log.Fatal(err)
	}
	metricRecordCount.WithLabelValues(name).Set(rl)
	metricLastRunTimestamp.Set(float64(time.Now().Unix()))
	metricRunDuration.Set(float64(*flagDuration))

	if !*flagWrite {
		promfmt.Fprint(os.Stdout, promfmt.NewPrefixFilter("csvne_"))
		return
	}

	if err := promfmt.WriteFile(promfile, promfmt.NewPrefixFilter("csvne_")); err != nil {
		log.Fatalf("Failed to write to prom file %q: %s", promfile, err)
	}
}

func records(name string) (float64, error) {
	f, err := os.Open(name)
	defer f.Close()
	if err != nil {
		return 0, fmt.Errorf("failed to open file: %q: %s", name, err)
	}

	records, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return 0, fmt.Errorf("failed to parse file: %q: %s", name, err)
	}
	j := 0
	for i := 0; i < len(records); i++ {
		j += len(records[i])
	}
	return float64(j), nil
}

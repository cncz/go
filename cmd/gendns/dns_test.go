package main

import (
	"bytes"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestConfigParse(t *testing.T) {
	dir := "testdata"
	testFiles, err := os.ReadDir(dir)
	if err != nil {
		t.Fatalf("could not read %s: %q", dir, err)
	}
	for _, f := range testFiles {
		if f.IsDir() {
			continue
		}
		if filepath.Ext(f.Name()) != ".gen" {
			continue
		}
		if !strings.HasPrefix(f.Name(), "dns.") {
			continue
		}
		buf, err := os.ReadFile("testdata/" + f.Name())
		if err != nil {
			t.Fatal(err)
		}
		expected, err := os.ReadFile("testdata/" + f.Name() + ".expected")
		if err != nil {
			t.Fatal(err)
		}
		expected = bytes.ReplaceAll(expected, []byte{' '}, []byte{'_'})

		*flagDry = true
		t.Run(f.Name(), func(t *testing.T) {
			conf, err := confParser.ParseBytes(f.Name(), buf)
			if err != nil {
				t.Fatal(err)
			}
			dns, err := makeDNS(conf)
			if err != nil {
				t.Fatal(err)
			}
			buf := &bytes.Buffer{}
			if err := dumpToFiles(dns, ";", buf); err != nil {
				t.Error(err)
				return
			}
			got := bytes.ReplaceAll(buf.Bytes(), []byte{' '}, []byte{'_'})

			if string(got) != string(expected) {
				t.Errorf("expected and generated files don't match")
				t.Logf("Got\n%s\n", got)
				t.Logf("Want\n%s\n", expected)
			}
		})
	}
}

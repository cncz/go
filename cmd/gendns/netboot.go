package main

import (
	"bytes"
	"fmt"
	"io"
)

type netboot struct {
	name    string
	seed    string
	dev     string
	distro  string
	part    string
	swap    string
	contact string
}

// makeNetBoot creats a NetBoot config snippet from the input.
func makeNetBoot(c *Config) (map[string]*bytes.Buffer, error) {
	prefix := c.Default(Prefix)
	data := map[string]*bytes.Buffer{}
	n := netboot{}
	oldSource := ""
	for _, b := range c.Blocks {
		if len(b.Elements) == 0 {
			// empty lines...
			continue
		}
		for _, e := range b.Elements {
			switch {
			case e.Name != nil:
				n.name = *e.Name
			case e.Seed != nil:
				n.seed = *e.Seed
			case e.Dev != nil:
				n.dev = *e.Dev
			case e.Distro != nil:
				n.distro = *e.Distro
			case e.Part != nil:
				n.part = *e.Part
			case e.Swap != nil:
				n.swap = *e.Swap
			case e.Contact != nil:
				n.contact = *e.Contact
			default:
				// EOL or other fields
			}
		}
		// if none are set, this host shouldn't be netbooted
		if n.seed == "" && n.dev == "" && n.distro == "" && n.part == "" && n.swap == "" {
			n = netboot{}
			continue
		}

		if IsDefault(n.name) {
			n = netboot{}
			continue
		}
		if n.swap == "" {
			n.swap = c.Default(Swap)
		}
		if n.swap == "" {
			return nil, fmt.Errorf("line %d, swap is not set and there is no default", b.Pos.Line)
		}
		if n.dev == "" {
			n.dev = c.Default(Dev)
		}
		if n.dev == "" {
			return nil, fmt.Errorf("line %d, dev is not set and there is no default", b.Pos.Line)
		}
		if n.distro == "" {
			n.distro = c.Default(Distro)
		}
		if n.distro == "" {
			return nil, fmt.Errorf("line %d, distro is not set and there is no default", b.Pos.Line)
		}
		if n.seed == "" {
			n.seed = c.Default(Seed)
		}
		if n.seed == "" {
			return nil, fmt.Errorf("line %d, seed is not set and there is no default", b.Pos.Line)
		}
		if n.part == "" {
			n.part = c.Default(Part)
		}
		if n.part == "" {
			return nil, fmt.Errorf("line %d, part is not set and there is no default", b.Pos.Line)
		}
		s, ok := data[prefix+"netboot"]
		if !ok {
			s = &bytes.Buffer{}
			data[prefix+"netboot"] = s
		}

		contact := func(s io.Writer) {
			if n.contact != "" {
				newlineIfNotFirst(s)
				contact(s, n.contact, "#")
				n.contact = ""
			}
		}
		source := func(s io.Writer) {
			if b.Source != oldSource {
				newlineIfNotFirst(s)
				source(s, b.Source, "#")
				oldSource = b.Source
			}
		}

		// we need all values, checked in Validate.
		// machine distribution  seed/partitioning device partitioning  swapsize password boot-menu  install-menu utility-menu
		// localhost ubuntu-22.04   server         sda      server      64G      true     false      true         false
		source(s)
		contact(s)
		fmt.Fprintf(s, "%s\t%s\t%s\t%s\t%s\t%s\ttrue false true false\n", n.name, n.distro, n.seed, n.dev, n.part, n.swap)
		n = netboot{}
	}
	return data, nil
}

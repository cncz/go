package main

import (
	"bytes"
	"encoding/json"
	"strconv"
)

type jOutput struct {
	Name     string `json:"name"`
	Ethernet string `json:"ethernet"`
	Distro   string `json:"distro"`
	Mem      uint64 `json:"mem"`
	Disk     uint64 `json:"disk"`
	Cpu      uint64 `json:"cpu"`
	Vm       string `json:"-"`
}

func makeVirt(c *Config) (map[string]*bytes.Buffer, error) {
	prefix := c.Default(Prefix)
	data := map[string]*bytes.Buffer{}
	j := jOutput{}
	for _, b := range c.Blocks {
		if len(b.Elements) == 0 {
			// empty lines...
			continue
		}
		for _, e := range b.Elements {
			switch {
			case e.Vm != nil:
				j.Vm = *e.Vm
			case e.Name != nil:
				j.Name = *e.Name
			case e.Ethernet != nil:
				j.Ethernet = *e.Ethernet
			case e.Distro != nil:
				j.Distro = *e.Distro
			case e.Mem != nil:
				j.Mem, _ = parseUnit(*e.Mem)
			case e.Disk != nil:
				j.Disk, _ = parseUnit(*e.Disk)
			case e.Cpu != nil:
				cpu, _ := strconv.ParseUint(*e.Cpu, 10, 32)
				j.Cpu = uint64(cpu)
			default:
				// EOL or other fields
			}
		}
		if IsDefault(j.Name) {
			j = jOutput{}
			continue
		}
		if j.Distro == "" {
			j.Distro = c.Default(Distro)
		}

		s, ok := data[prefix+"vm."+j.Vm+".json"]
		if !ok {
			s = &bytes.Buffer{}
			data[prefix+"vm."+j.Vm+".json"] = s
			s.Write([]byte("[\n")) // opening json list
		}
		buf, err := json.Marshal(j)
		if err != nil {
			return nil, err
		}
		// hack to make this a proper json list
		if s.Len() > 4 {
			s.Truncate(s.Len() - 3) // remove the \n]\n written below
			s.Write([]byte(",\n"))  // write comma, newline
		}
		s.Write(buf)
		s.Write([]byte("\n]\n"))
		j = jOutput{}
	}
	return data, nil
}

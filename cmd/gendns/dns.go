package main

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"strings"
)

type dns struct {
	name    string
	cpu     string
	os      string
	cname   string
	contact string
	ttl     string
	addr    []string
}

// makeDNS creats a DNS config snippet from the input.
func makeDNS(c *Config) (map[string]*bytes.Buffer, error) {
	prefix := c.Default(Prefix)
	data := map[string]*bytes.Buffer{} // filename -> buffer
	d := dns{}
	oldSource := ""
	for _, b := range c.Blocks {
		if len(b.Elements) == 0 { // comments
			continue
		}
		for _, e := range b.Elements {
			switch {
			case e.Name != nil:
				d.name = *e.Name
			case e.Addr != nil:
				d.addr = append(d.addr, *e.Addr)
			case e.Cname != nil:
				d.cname = *e.Cname
			case len(e.Hinfo) > 0:
				d.cpu, d.os = e.Hinfo[0], e.Hinfo[1]
			case e.Contact != nil:
				d.contact = *e.Contact
			case e.Ttl != nil:
				d.ttl = *e.Ttl
			default:
				// EOL or other fields
			}
		}
		if IsDefault(d.name) {
			d = dns{}
			continue
		}
		s, ok := data[prefix+zone(d.name)]
		if !ok {
			s = &bytes.Buffer{}
			data[prefix+zone(d.name)] = s
		}
		contact := func(s io.Writer) {
			if d.contact != "" {
				newlineIfNotFirst(s)
				contact(s, d.contact, ";")
				d.contact = ""
			}
		}
		source := func(s io.Writer) {
			if b.Source != oldSource {
				newlineIfNotFirst(s)
				source(s, b.Source, ";")
				oldSource = b.Source
			}
		}

		for _, a := range d.addr {
			i := net.ParseIP(a)
			rec := "AAAA"
			if i.To4() != nil {
				rec = "A"
			}
			source(s)
			contact(s)
			fmt.Fprintf(s, "%s\t\t%s\tIN\t\t%s\t%s\n", fqdn(d.name), d.ttl, rec, a)
		}
		if d.cname != "" {
			source(s)
			contact(s)
			fmt.Fprintf(s, "%s\t\t%s\tIN\t\tCNAME\t%s\n", fqdn(d.name), d.ttl, fqdn(d.cname))
		}
		if d.cpu != "" || d.os != "" {
			source(s)
			contact(s)
			fmt.Fprintf(s, "%s\t\t%s\tIN\t\tHINFO\t%s %s\n", fqdn(d.name), d.ttl, quote(d.cpu), quote(d.os))
		}
		d = dns{}
	}
	return data, nil
}

func fqdn(s string) string {
	if s == "" {
		return "."
	}
	if s[len(s)-1] != '.' {
		return s + "."
	}
	return s
}

// zone returns the zone name for s, this is everything after the first dot.
func zone(s string) string {
	i := strings.Index(s, ".")
	if i == 0 {
		return s
	}
	// dot at end can't happen, because we pre validate this
	return s[i+1:]
}

func quote(s string) string {
	if s[0] == '"' {
		return s
	}
	return `"` + s + `"`
}

%%%
title = "gendns 1"
area = "User Commands"
workgroup = "C&CZ"
%%%

gendns
=====

## Name

gendns - generate DHCP, DNS and netboot config from a simple configuration file

## Synopsis

`gendns` *OPTIONS* *FILE...*

## Description

Gendns parses a configuration file and then generated DHCP, DNS, or netboot configuration files.

The followng options are supported:

**-n**
: when enabled, do a dry run and output everything to standard output

**-t** *TYPE*
: generate config for type *TYPE*: 'dns', 'dhcp', 'netboot' or 'virt'. Not specified or *TYPE* is
'all' means all types.

### Configuration File

At a high level the config consists out of *block*s and *comment*s. A comment is started with
a `#` on a line. Each *block* is delimited with braces: `{ i am a block }`. A block may be split
onto multiple lines.

Inside a block there is a list of keys and values, the order of these is irrelevant. A key without a
value is used for the DNS host name of this block, there can only be one of these per block.
If a key has white space it should be quoted. Keys and values are separated with white space.

As said a bare key in a block is the name of the host.

Supported keys with values are:

For meta/machine settings:

`contact` *TEXT*
: define the owner for this piece of information, the text is free-form

`comment` *TEXT*
: an optional comment or description for this server

`fin` *TEXT*
: define the financiel information; usually in the form of a URL, but this is not enforced

`vm` *MACHINE*
: this machine is a virtual machine and should run on *MACHINE*

`disk` *SIZE[MGT]*
: disk size of machine in Mega, Giga or Tera bytes

`cpu` *AMOUNT*
: number of CPUs for this machine

`mem` *SIZE[MGT]*
: amount of memory for this machine, Mega, Giga or Tera bytes

Currently none of these fields end up in any of the generated files.

For location settings:

`site` *SITE*
: define the site for this host, i.e. 'huygens', can have a default setting

`location` *LOCATION*
: define the location of this machine within `site`

`rack` *RACK*
: define the rack for this host in the `location`

For DHCP settings:

`ethernet` *MACADDRESS*
: define *MACADDRESS* for this host name

For DNS settings:

`addr` *IP*
: define *I4* as the A (IPv4) or AAAA (IPv6) record for this host name, this may be repeated

`hinfo` *CPU* *OS*
: define an HINFO record this this host

`cname` *CNAME*
: define a CNAME record this this host

`ttl` *TTL*
: define an (optional) TTL for this DNS name, can have a default

Note that each of these keywords can be repeated, if done, the **latest** value will be used. Only
`addr` can actually used multiple times

For netboot/machine settings:

`seed` *SEED*
: define *SEED* as the netboot seed for this host, usually "client" or "server", can have a default
  setting

`dev` *DEVICE*
: define *DEVICE* as the netboot boot device, can have a default setting

`distro` *DISTRO*
: define the distribution this this host, can have a default setting, also used for VMs

`part` *SCHEME*
: define the partitioning scheme for this host, can have a default setting

`swap` *SIZE*
: define the swap size, size should have the 'G' suffix for Gigabyte, can have a default setting

At least _one of these needs to be set for netboot configuration to be generated_ for this host.

### Defaults

It is possible to set defaults for the netboot settings (mostly netboot, but also others).  This is
done by using the special host name `default` and then defining settings just like any other block:
(Note: `default` is otherwise an illegal name, because it doesn't contain any dots).

~~~
{ default swap 64G distro ubuntu-22.04 }
~~~

Sets the default swap size and distribution. Unlike other blocks the `default` block may be repeated.
If settings are duplicated in other blokcs, only the *first* one will be used. Also note that
defaults for DNS and DHCP are accepted, but not used.

### Default Keys

Some keys can only be (effectively) used in a `{ default }` "host", these include:

`prefix` *PREFIX*
: set *PREFIX* to be used to as the prefix for the file names that are written.

## Files Names

Gendns writes multiple configuration files. The names of these files is fully dependent on the
input.

- DNS files are named after the `<prefix><zone>`, if prefix is `hfml-` and the zone is
  `net.science.ru.nl` for some names in the config, a `hfml-net.science.ru.nl` file name is used.

- DHCP are named after `<prefix><distro>` so if distro is set `ubuntu-22.04` and the prefix is the
  same `hfml-ubuntu-22.04`. If distro isn't set and there isn't a default `dhcp` is used, resulting
  in `hfml-dhcp`.

- For netboot `<prefix>netboot` is written.

- For virtual machine `<prefix>vm.<hostmachine>.json` is written in JSON format.

## Examples

This defines a host `www.science.ru.nl` with a MAC address and IP

~~~ txt
{ www.science.ru.nl ethernet 00:40:ab:27:3b:c6 addr 192.168.1.1 }
~~~

This generates two config snippets:

~~~ dhcp
host www { hardware ethernet 00:40:ab:27:3b:c6; fixed-address www.science.ru.nl; }
~~~
and for DNS:
~~~ dns
www.science.ru.nl.		IN	A	192.168.1.1
~~~

And here we add a HINFO record:

~~~ txt
# this is a comment
{ www.science.ru.nl ethernet 00:40:ab:27:3b:c6 addr 192.168.1.1 addr 127.0.0.1 hinfo dell "linux-ubuntu" }
~~~

will generate:

~~~ dhcp
host www { hardware ethernet 00:40:ab:27:3b:c6; fixed-address www.science.ru.nl; }
~~~
and for DNS:
~~~ dns
www.science.ru.nl.		IN	A	    192.168.1.1
www.science.ru.nl.		IN	A	    127.0.0.1
www.science.ru.nl.		IN	HINFO	"dell" "linux-ubuntu"
~~~

package main

import (
	"bytes"
	"fmt"
	"io"
	"strings"
)

type dhcp struct {
	name    string
	mac     string
	contact string
	distro  string // if empty default to 'dhcp'
}

// makeDHCP creats a DHCP config snippet from the input.
func makeDHCP(c *Config) (map[string]*bytes.Buffer, error) {
	prefix := c.Default(Prefix)
	oldSource := ""
	data := map[string]*bytes.Buffer{}
	d := dhcp{}
	for _, b := range c.Blocks {
		if len(b.Elements) == 0 { // comments
			continue
		}
		// create a line like this:
		//   host pdu-kast114-03-pdu1 { hardware ethernet 28:29:86:1a:16:a9; fixed-address pdu-kast114-03-pdu1.net.science.ru.nl; }
		// from this:
		// { ds3-huyg.net.science.ru.nl ethernet 00:30:ab:28:3b:c6 addr 192.168.1.1 }
		for _, e := range b.Elements {
			switch {
			case e.Name != nil:
				d.name = *e.Name
			case e.Ethernet != nil:
				d.mac = *e.Ethernet
			case e.Distro != nil:
				d.distro = *e.Distro
			case e.Contact != nil:
				d.contact = *e.Contact
			default:
				// EOL or other fields
			}
		}
		if IsDefault(d.name) {
			d = dhcp{}
			continue
		}
		if d.distro == "" {
			d.distro = c.Default(Distro)
		}
		if d.distro == "" {
			d.distro = "dhcp"
		}
		contact := func(s io.Writer) {
			if d.contact != "" {
				newlineIfNotFirst(s)
				contact(s, d.contact, "#")
				d.contact = ""
			}
		}
		source := func(s io.Writer) {
			if b.Source != oldSource {
				newlineIfNotFirst(s)
				source(s, b.Source, "#")
				oldSource = b.Source
			}
		}

		if d.name != "" && d.mac != "" {
			s, ok := data[prefix+d.distro]
			if !ok {
				s = &bytes.Buffer{}
				data[prefix+d.distro] = s
			}
			source(s)
			contact(s)
			fmt.Fprintf(s, "host %s { hardware ethernet %s; fixed-address %s; }\n", dhcpHost(d.name), strings.ToLower(d.mac), d.name)
		}
		d = dhcp{}
	}
	return data, nil
}

func dhcpHost(s string) string {
	f := strings.Split(s, ".")
	if len(f) > 0 {
		return f[0]
	}
	return ""
}

" Quit when a syntax file was already loaded.
if exists('b:current_syntax') | finish | endif

syntax keyword simpleCommand addr ethernet hinfo distro swap dev part seed
syn region simpleComment oneline start='\%(^\|\s\+\)#' end='$'

hi def link simpleCommand Function
hi def link simpleComment Comment

let b:current_syntax = 'simple'

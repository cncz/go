package main

import (
	"bytes"
	"fmt"
	"net"
	"strconv"
	"strings"

	"github.com/alecthomas/participle/v2"
	"github.com/alecthomas/participle/v2/lexer"
)

var (
	confLexer = lexer.MustSimple([]lexer.SimpleRule{
		{"Comment", `#[^\n]*`},
		{"Contact", `\bcontact\b`},
		{"Description", `\bdescription\b`},
		{"Site", `\bsite\b`},
		{"Location", `\blocation\b`},
		{"Rack", `\brack\b`},
		{"Addr", `\baddr\b`},
		{"Cname", `\bcname\b`},
		{"Ttl", `\bttl\b`},
		{"Ethernet", `\bethernet\b`},
		{"Hinfo", `\bhinfo\b`},
		{"Distro", `\bdistro\b`},
		{"Seed", `\bseed\b`},
		{"Dev", `\bdev\b`},
		{"Part", `\bpart\b`},
		{"Swap", `\bswap\b`},
		{"Prefix", `\bprefix\b`},
		{"Vm", `\bvm\b`},
		{"Mem", `\bmem\b`},
		{"Disk", `\bdisk\b`},
		{"Cpu", `\bcpu\b`},
		{"Fin", `\bfin\b`},
		{"String", `"(\\"|[^"])*"`},
		{"Ident", `[a-zA-Z_:0-9.\-]+`},
		{"Punct", `[{},]`},
		{"whitespace", `[ \t]+`},
		{"EOL", `[\n\r]+`},
	})

	confParser = participle.MustBuild[Config](
		participle.Lexer(confLexer),
		participle.CaseInsensitive("Ident"),
		participle.UseLookahead(2),
	)
)

// potentially make this much smarter and detect the keywords as well?
// for now we iterate over the parsed conf and do it in Go code.
type Config struct {
	Pos      lexer.Position
	Defaults []*Block
	Blocks   []*Block `@@*`
}

type Block struct {
	Pos      lexer.Position
	Source   string     // source file we parsed to create this Block
	Comment  string     `  @Comment EOL`
	Elements []*Element `| "{" @@* "}" EOL`
}

type Element struct {
	Pos         lexer.Position
	Addr        *string  `  Addr EOL? (@Ident | @String | EOL)`                           // DNS: A record
	Cname       *string  `| Cname EOL? (@Ident | @String | EOL)`                          // DNS: CNAME record
	Ttl         *string  `| Ttl EOL? (@Ident | @String | EOL)`                            // DNS: TTL
	Hinfo       []string `| Hinfo EOL? (@Ident | @String | EOL) (@Ident | @String | EOL)` // DNS: HINFO record
	Ethernet    *string  `| Ethernet EOL? (@Ident | @String | EOL )`                      // DHCP: Mac Address
	Distro      *string  `| Distro EOL? (@Ident | @String | EOL )`                        // Netboot: distro
	Seed        *string  `| Seed EOL? (@Ident | @String | EOL )`                          // Netboot type: client or server
	Dev         *string  `| Dev EOL? (@Ident | @String | EOL )`                           // Netboot: boot device
	Part        *string  `| Part EOL? (@Ident | @String | EOL )`                          // Netboot: partioning scheme
	Swap        *string  `| Swap EOL? (@Ident | @String | EOL )`                          // Netboot: swap size
	Prefix      *string  `| Prefix EOL? (@Ident | @String | EOL )`                        // *: file prefix
	Contact     *string  `| Contact EOL? (@Ident | @String | EOL )`                       // machine: contact
	Description *string  `| Description EOL? (@Ident | @String | EOL )`                   // machine: description
	Site        *string  `| Site EOL? (@Ident | @String | EOL )`                          // machine: site
	Location    *string  `| Location EOL? (@Ident | @String | EOL )`
	Rack        *string  `| Rack EOL? (@Ident | @String | EOL )`
	Vm          *string  `| Vm EOL? (@Ident | @String | EOL )`   // machine: host
	Mem         *string  `| Mem EOL? (@Ident | @String | EOL )`  // machine: mem size
	Disk        *string  `| Disk EOL? (@Ident | @String | EOL )` // machine: disk size
	Cpu         *string  `| Cpu EOL? (@Ident | @String | EOL )`  // machine: number of cpu
	Fin         *string  `| Fin EOL? (@Ident | @String | EOL )`  // machine: URL to financial documents
	Name        *string  `| (@Ident | @String | EOL)`
}

type tokenType int

const (
	Name tokenType = iota
	Addr
	Cname
	Ttl
	Ethernet
	Hinfo
	Distro
	Seed
	Dev
	Part
	Swap
	Prefix
	Contact
	Description
	Site
	Location
	Rack
	Vm
	Disk
	Mem
	Cpu
	Fin
)

func (c *Config) String() string {
	s := &bytes.Buffer{}
	for i, b := range c.Blocks {
		if i > 0 {
			fmt.Fprintln(s)
		}
		if len(b.Elements) == 0 {
			fmt.Fprintf(s, "%s\n", b.Comment)
			continue
		}
		fmt.Fprintf(s, "{ ")
		for _, e := range b.Elements {
			switch {
			case e.Name != nil:
				fmt.Fprintf(s, "%s\n", strings.ToLower(*e.Name))
			}
		}
		// group addr
		addr := false
		for _, e := range b.Elements {
			switch {
			case e.Addr != nil:
				if !addr {
					fmt.Fprint(s, "   ")
					addr = true
				}
				fmt.Fprintf(s, "addr %s", *e.Addr)
				fmt.Fprint(s, "  ")
			}
		}
		if addr {
			fmt.Fprint(s, "\n")
		}
		for _, e := range b.Elements {
			switch {
			case e.Name != nil:
				// done above
				continue
			case e.Addr != nil:
				// done above
				continue
			case e.Ethernet != nil:
				fmt.Fprintf(s, "   ethernet %s\n", strings.ToLower(*e.Ethernet))
			case len(e.Hinfo) > 0:
				fmt.Fprintf(s, "   hinfo %s %s\n", e.Hinfo[0], e.Hinfo[1])
			case e.Vm != nil:
				fmt.Fprintf(s, "   vm %s\n", *e.Vm)
			case e.Description != nil:
				fmt.Fprintf(s, "   decription %s\n", *e.Description)
			case e.Site != nil:
				fmt.Fprintf(s, "   site %s\n", *e.Site)
			case e.Disk != nil:
				fmt.Fprintf(s, "   disk %s\n", *e.Disk)
			case e.Mem != nil:
				fmt.Fprintf(s, "   mem %s\n", *e.Mem)
			case e.Cpu != nil:
				fmt.Fprintf(s, "   cpu %s\n", *e.Cpu)
			case e.Ttl != nil:
				fmt.Fprintf(s, "   ttl %s\n", *e.Ttl)
			case e.Dev != nil:
				fmt.Fprintf(s, "   dev %s\n", *e.Dev)
			case e.Distro != nil:
				fmt.Fprintf(s, "   distro %s\n", *e.Distro)
			case e.Part != nil:
				fmt.Fprintf(s, "   part %s\n", *e.Part)
			case e.Swap != nil:
				fmt.Fprintf(s, "   swap %s\n", *e.Swap)
			case e.Seed != nil:
				fmt.Fprintf(s, "   seed %s\n", *e.Seed)
			case e.Fin != nil:
				fmt.Fprintf(s, "   fin %s\n", *e.Fin)
			case e.Prefix != nil:
				fmt.Fprintf(s, "   prefix %s\n", *e.Prefix)
			case e.Site != nil:
				fmt.Fprintf(s, "   site %s\n", *e.Site)
			case e.Location != nil:
				fmt.Fprintf(s, "   location %s\n", *e.Location)
			case e.Rack != nil:
				fmt.Fprintf(s, "   rack %s\n", *e.Rack)
				// double check if we have all elements
			}
		}
		fmt.Fprintf(s, "}\n")
	}
	return s.String()
}

func (c *Config) Validate() error {
	for _, b := range c.Blocks {
		if len(b.Elements) == 0 { // comments
			continue
		}
		m := map[tokenType]string{}
		for _, e := range b.Elements {
			switch {
			case e.Name != nil:
				if m[Name] != "" && m[Name] != "default" {
					return fmt.Errorf("line %d, %q is already set as name, can't use: %s, or unknown keyword: %s", e.Pos.Line, m[Name], *e.Name, *e.Name)
				}
				if (*e.Name)[len(*e.Name)-1] == '.' {
					return fmt.Errorf("line %d, name should not end in a dot: %s", e.Pos.Line, *e.Name)
				}
				if strings.Count(*e.Name, ".") == 0 && *e.Name != "default" {
					return fmt.Errorf("line %d, name should have at least a dot: %s", e.Pos.Line, *e.Name)
				}
				m[Name] = *e.Name
			case e.Addr != nil:
				ip := net.ParseIP(*e.Addr)
				if ip == nil {
					return fmt.Errorf("line %d, %q should be a valid IPv4 address", e.Pos.Line, *e.Addr)
				}
				if ip.To16() == nil {
					return fmt.Errorf("line %d, %q should be an IPv4 address", e.Pos.Line, *e.Addr)
				}
				m[Addr] = *e.Addr
			case e.Cname != nil:
				if strings.Count(*e.Cname, ".") == 0 {
					return fmt.Errorf("line %d, name should have at least a dot: %s", e.Pos.Line, *e.Cname)
				}
				if (*e.Cname)[len(*e.Cname)-1] == '.' {
					return fmt.Errorf("line %d, name should not end in a dot: %s", e.Pos.Line, *e.Cname)
				}
				m[Cname] = *e.Cname
			case e.Ttl != nil:
				if _, ok := parseTTL(*e.Ttl); !ok {
					return fmt.Errorf("line %d, illegal TTL: %s", e.Pos.Line, *e.Ttl)
				}
				m[Ttl] = *e.Ttl
			case len(e.Hinfo) > 0:
				if m[Hinfo] != "" {
					return fmt.Errorf("line %d, %q is already set as hinfo, can't use: %s", e.Pos.Line, m[Hinfo], e.Hinfo[0])
				}
				m[Hinfo] = e.Hinfo[0]
			case e.Vm != nil:
				if m[Vm] != "" {
					return fmt.Errorf("line %d, %q is already set as vm, can't use: %s", e.Pos.Line, m[Vm], *e.Vm)
				}
				m[Vm] = *e.Vm
			case e.Mem != nil:
				if m[Mem] != "" {
					return fmt.Errorf("line %d, %q is already set as mem, can't use: %s", e.Pos.Line, m[Mem], *e.Mem)
				}
				if _, ok := parseUnit(*e.Mem); !ok {
					return fmt.Errorf("line %d, illegal mem: %s", e.Pos.Line, *e.Mem)
				}
				m[Mem] = *e.Mem
			case e.Disk != nil:
				if m[Disk] != "" {
					return fmt.Errorf("line %d, %q is already set as disk, can't use: %s", e.Pos.Line, m[Mem], *e.Mem)
				}
				if _, ok := parseUnit(*e.Disk); !ok {
					return fmt.Errorf("line %d, illegal disk: %s", e.Pos.Line, *e.Disk)
				}
				m[Disk] = *e.Disk
			case e.Cpu != nil:
				if m[Cpu] != "" {
					return fmt.Errorf("line %d, %q is already set as disk, can't use: %s", e.Pos.Line, m[Cpu], *e.Cpu)
				}
				if _, err := strconv.ParseUint(*e.Cpu, 10, 64); err != nil {
					return fmt.Errorf("line %d, illegal cpu: %s", e.Pos.Line, *e.Cpu)
				}
				m[Cpu] = *e.Cpu
			case e.Ethernet != nil:
				if m[Ethernet] != "" {
					return fmt.Errorf("line %d, %q is already set as ethernet, can't use: %s", e.Pos.Line, m[Ethernet], *e.Ethernet)
				}
				if _, err := net.ParseMAC(*e.Ethernet); err != nil {
					return err
				}
				m[Ethernet] = *e.Ethernet
			// Netboot items
			case e.Swap != nil:
				if m[Swap] != "" {
					return fmt.Errorf("line %d, %q is already set as hinfo, can't use: %s", e.Pos.Line, m[Swap], *e.Swap)
				}
				if _, ok := parseUnit(*e.Swap); !ok {
					return fmt.Errorf("line %d, illegal swap: %s", e.Pos.Line, *e.Disk)
				}
				m[Swap] = *e.Swap
			case e.Location != nil:
				m[Location] = *e.Location
			case e.Site != nil:
				m[Site] = *e.Site
			case e.Rack != nil:
				m[Rack] = *e.Rack
			default:
				// EOL
				continue
			}
		}
		if m[Name] == "" {
			return fmt.Errorf("line %d, no name set in block", b.Pos.Line)
		}
		if IsDefault(m[Name]) { // copy to Defaults
			c.Defaults = append(c.Defaults, b)
			continue // skip other checks
		}
		if m[Cname] != "" {
			if m[Addr] != "" {
				return fmt.Errorf("line %d, CNAME and other data: %s", b.Pos.Line, m[Cname])
			}
		}
		if m[Ethernet] != "" {
			if m[Addr] == "" {
				return fmt.Errorf("line %d, ethernet set, but no addr defined: %s", b.Pos.Line, m[Ethernet])
			}
		}
		if m[Vm] != "" {
			if m[Location] != "" {
				return fmt.Errorf("line %d, if %q is set, location: %q can not also be set", b.Pos.Line, "vm", m[Location])
			}
			if m[Site] != "" {
				return fmt.Errorf("line %d, if %q is set, site: %q can not also be set", b.Pos.Line, "vm", m[Location])
			}
			if m[Rack] != "" {
				return fmt.Errorf("line %d, if %q is set, rack: %q can not also be set", b.Pos.Line, "vm", m[Location])
			}
		} else {
			if m[Location] == "" {
				return fmt.Errorf("line %d, %q must be set", b.Pos.Line, "location")
			}
			if m[Site] == "" {
				return fmt.Errorf("line %d, %q must be set", b.Pos.Line, "site")
			}
			if m[Rack] == "" {
				return fmt.Errorf("line %d, %q must be set", b.Pos.Line, "rack")
			}
		}
	}
	return nil
}

func IsDefault(s string) bool { return s == "default" }

func (c *Config) Default(t tokenType) string {
	for _, d := range c.Defaults {
		for _, e := range d.Elements {
			switch t {
			case Distro:
				if e.Distro != nil {
					return *e.Distro
				}
			case Seed:
				if e.Seed != nil {
					return *e.Seed
				}
			case Dev:
				if e.Dev != nil {
					return *e.Dev
				}
			case Part:
				if e.Part != nil {
					return *e.Part
				}
			case Swap:
				if e.Swap != nil {
					return *e.Swap
				}
			case Prefix:
				if e.Prefix != nil {
					return *e.Prefix
				}
			case Site:
				if e.Site != nil {
					return *e.Site
				}
			case Ttl:
				if e.Ttl != nil {
					return *e.Ttl
				}
			}
		}
	}
	return ""
}

func parseTTL(ttl string) (uint32, bool) {
	var s, i uint32
	for _, c := range ttl {
		switch c {
		case 's', 'S':
			s += i
			i = 0
		case 'm', 'M':
			s += i * 60
			i = 0
		case 'h', 'H':
			s += i * 60 * 60
			i = 0
		case 'd', 'D':
			s += i * 60 * 60 * 24
			i = 0
		case 'w', 'W':
			s += i * 60 * 60 * 24 * 7
			i = 0
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			i *= 10
			i += uint32(c) - '0'
		default:
			return 0, false
		}
	}
	return s + i, true
}

func parseUnit(mem string) (uint64, bool) {
	// <num><unit>
	unit := mem[len(mem)-1]
	val := mem[:len(mem)-1]
	amount, err := strconv.ParseUint(val, 10, 64)
	if err != nil {
		return 0, false
	}

	switch unit {
	case 'M':
		return uint64(amount) * 1_000_000, true
	case 'G':
		return uint64(amount) * 1_000_000_000, true
	case 'T':
		return uint64(amount) * 1_000_000_000_000, true
	default:
		return 0, false
	}
}

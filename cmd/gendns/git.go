package main

import (
	"bytes"
	"context"
	"os/exec"
)

// lsFile return the relative path of name inside the git repository.
func lsFile(name string) string {
	out, err := run("ls-files", "--full-name", name)
	if err != nil {
		return ""
	}
	return string(out)
}

func revParse(ref string) string {
	out, err := run("rev-parse", ref)
	if err != nil {
		return ""
	}
	return string(out)
}

func run(args ...string) ([]byte, error) {
	ctx := context.TODO()
	cmd := exec.CommandContext(ctx, "git", args...)
	cmd.Env = []string{"GIT_CONFIG_GLOBAL=/dev/null", "GIT_CONFIG_SYSTEM=/dev/null"}
	out, err := cmd.CombinedOutput()
	return bytes.TrimSpace(out), err
}

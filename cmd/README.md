# Cmds

These directories hold Go commands. Each command should have a README/manual page to document it.

## NodeExporter Exporters

These programs live in the `*ne` sub directories, each binary have some common properties:

Generally we want node-exporter exporter to follow the following rules:

- accept a -t <duration> flag to tells it how often we expect it to run. This flag's value in only
  used in the metrics.
- accept a -w flag that tells it to write to `/var/lib/prometheus/node-exporter/<basename>.prom`
  `-w=false` just writes to standard output.
- at least have the following metrics:
    - `<name>_last_run_timestamp_seconds <epoch>` - when did we run
    - `<name>_run_duration_seconds <duration>` -  the value of -t

All *NE*s are packaged in cncz-ne package.

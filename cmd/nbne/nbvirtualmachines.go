package main

import (
	"time"
)

type NbVirtualmachines struct {
	Count    int         `json:"count"`
	Next     string      `json:"next"`
	Previous interface{} `json:"previous"`
	Results  []struct {
		ID      int    `json:"id"`
		URL     string `json:"url"`
		Display string `json:"display"`
		Name    string `json:"name"`
		Status  struct {
			Value string `json:"value"`
			Label string `json:"label"`
		} `json:"status"`
		Site    interface{} `json:"site"`
		Cluster struct {
			ID      int    `json:"id"`
			URL     string `json:"url"`
			Display string `json:"display"`
			Name    string `json:"name"`
		} `json:"cluster"`
		Device           interface{}   `json:"device"`
		Role             interface{}   `json:"role"`
		Tenant           interface{}   `json:"tenant"`
		Platform         interface{}   `json:"platform"`
		PrimaryIP        interface{}   `json:"primary_ip"`
		PrimaryIP4       interface{}   `json:"primary_ip4"`
		PrimaryIP6       interface{}   `json:"primary_ip6"`
		Vcpus            float64       `json:"vcpus"`
		Memory           int           `json:"memory"`
		Disk             int           `json:"disk"`
		Comments         string        `json:"comments"`
		LocalContextData interface{}   `json:"local_context_data"`
		Tags             []interface{} `json:"tags"`
		CustomFields     struct {
		} `json:"custom_fields"`
		ConfigContext struct {
		} `json:"config_context"`
		Created     time.Time `json:"created"`
		LastUpdated time.Time `json:"last_updated"`
	} `json:"results"`
}

package main

import (
	"time"
)

type NbDevices struct {
	Count    int         `json:"count"`
	Next     string      `json:"next"`
	Previous interface{} `json:"previous"`
	Results  []struct {
		ID         int    `json:"id"`
		URL        string `json:"url"`
		Display    string `json:"display"`
		Name       string `json:"name"`
		DeviceType struct {
			ID           int    `json:"id"`
			URL          string `json:"url"`
			Display      string `json:"display"`
			Manufacturer struct {
				ID      int    `json:"id"`
				URL     string `json:"url"`
				Display string `json:"display"`
				Name    string `json:"name"`
				Slug    string `json:"slug"`
			} `json:"manufacturer"`
			Model string `json:"model"`
			Slug  string `json:"slug"`
		} `json:"device_type"`
		DeviceRole struct {
			ID      int    `json:"id"`
			URL     string `json:"url"`
			Display string `json:"display"`
			Name    string `json:"name"`
			Slug    string `json:"slug"`
		} `json:"device_role"`
		Tenant   interface{} `json:"tenant"`
		Platform struct {
			ID      int    `json:"id"`
			URL     string `json:"url"`
			Display string `json:"display"`
			Name    string `json:"name"`
			Slug    string `json:"slug"`
		} `json:"platform"`
		Serial   string      `json:"serial"`
		AssetTag interface{} `json:"asset_tag"`
		Site     struct {
			ID      int    `json:"id"`
			URL     string `json:"url"`
			Display string `json:"display"`
			Name    string `json:"name"`
			Slug    string `json:"slug"`
		} `json:"site"`
		Location struct {
			ID      int    `json:"id"`
			URL     string `json:"url"`
			Display string `json:"display"`
			Name    string `json:"name"`
			Slug    string `json:"slug"`
			Depth   int    `json:"_depth"`
		} `json:"location"`
		Rack struct {
			ID      int    `json:"id"`
			URL     string `json:"url"`
			Display string `json:"display"`
			Name    string `json:"name"`
		} `json:"rack"`
		Position float64 `json:"position"`
		Face     struct {
			Value string `json:"value"`
			Label string `json:"label"`
		} `json:"face"`
		ParentDevice interface{} `json:"parent_device"`
		Status       struct {
			Value string `json:"value"`
			Label string `json:"label"`
		} `json:"status"`
		Airflow          interface{}   `json:"airflow"`
		PrimaryIP        interface{}   `json:"primary_ip"`
		PrimaryIP4       interface{}   `json:"primary_ip4"`
		PrimaryIP6       interface{}   `json:"primary_ip6"`
		Cluster          interface{}   `json:"cluster"`
		VirtualChassis   interface{}   `json:"virtual_chassis"`
		VcPosition       interface{}   `json:"vc_position"`
		VcPriority       interface{}   `json:"vc_priority"`
		Comments         string        `json:"comments"`
		LocalContextData interface{}   `json:"local_context_data"`
		Tags             []interface{} `json:"tags"`
		CustomFields     struct {
			OrderNr          interface{} `json:"order_nr"`
			HasProblems      interface{} `json:"has_problems"`
			Fqdn             interface{} `json:"fqdn"`
			Billing          string      `json:"billing"`
			CheckDate        interface{} `json:"check_date"`
			ContractEndDate  string      `json:"contract_end_date"`
			Contracts        []string    `json:"contracts"`
			Description      interface{} `json:"description"`
			Functie          string      `json:"functie"`
			IntentionalState string      `json:"intentional_state"`
			OcsID            string      `json:"ocs_id"`
			Plan             interface{} `json:"plan"`
			Prio             interface{} `json:"prio"`
			PurchaseDate     string      `json:"purchase_date"`
			PurchasedBy      interface{} `json:"purchased_by"`
			RtID             string      `json:"rt_id"`
			Specs            string      `json:"specs"`
			VisibleLabel     string      `json:"visible_label"`
		} `json:"custom_fields"`
		ConfigContext struct {
		} `json:"config_context"`
		Created     time.Time `json:"created"`
		LastUpdated time.Time `json:"last_updated"`
	} `json:"results"`
}

package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	metricLastRunTimestamp = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "nbne_last_run_time_seconds",
		Help: "Epoch timestamp of the last run.",
	})
	metricRunDuration = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "nbne_run_duration_seconds",
		Help: "Gauge of current configured loop time.",
	})
	metricSite = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "nbne_site",
		Help: "Netbox site (building).",
	}, []string{"name"})
	metricLocation = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "nbne_location",
		Help: "Netbox location (server room name).",
	}, []string{"name"})
	metricRack = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "nbne_rack",
		Help: "Netbox rack (value is the rack unit / height ).",
	}, []string{"name"})
	metricCluster = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "nbne_cluster",
		Help: "Netbox cluster (vmhost).",
	}, []string{"name"})

	// nbnemeta
	metricMetaLastRunTimestamp = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "nbnemeta_last_run_timestamp_seconds",
		Help: "Epoch timestamp of the last run.",
	})
	metricMetaRunDuration = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "nbnemeta_run_duration_seconds",
		Help: "Gauge of current configured loop time.",
	})
	metricMetaPromRequestedCount = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "nbnemeta_prom_requested_count_total",
		Help: "Number of prom files written.",
	})
	metricMetaPromWrittenCount = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "nbnemeta_prom_written_count_total",
		Help: "Number of prom files written.",
	})
	metricMetaPromMissing = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "nbnemeta_prom_missing_count_total",
		Help: "Number of prom files missing.",
	})
)

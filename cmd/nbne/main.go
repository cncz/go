package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"

	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"go.science.ru.nl/log"
	"go.science.ru.nl/promfmt"
)

var (
	flagWrite        = flag.Bool("w", true, "write files in output directory")
	flagDuration     = flag.Uint("t", 3600, "default duration to export in seconds")
	flagDebug        = flag.Bool("d", false, "enable debug logging")
	flagOutputdir    = flag.String("o", "/tmp/nbne", "output directory")
	flagPromMetaPath = flag.String("m", "/var/lib/prometheus/node-exporter/nbnemeta.prom", "prom file path for writing nbnemeta data to")
)

type application struct {
	nb struct {
		host string
		key  string
	}
}

func main() {
	flag.Parse()
	if *flagDebug {
		log.D.Set()
	}

	// check output dir
	fi, err := os.Lstat(*flagOutputdir)
	if err != nil {
		log.Fatal(err)
	}
	if !fi.IsDir() {
		log.Fatal("Specified output directory is not a directory")
	}

	app := new(application)

	app.nb.host = os.Getenv("NETBOX_HOST")
	app.nb.key = os.Getenv("NETBOX_KEY")

	if app.nb.host == "" {
		log.Fatal("NETBOX_HOST must be provided")
	}

	if app.nb.key == "" {
		log.Fatal("NETBOX_KEY must be provided")
	}

	hosts := make(map[string]bool)
	s := bufio.NewScanner(os.Stdin)
	for s.Scan() {
		hosts[s.Text()] = true
	}

	// report total requested hosts
	metricMetaPromRequestedCount.Set(float64(len(hosts)))

	client := &http.Client{}
	var promwrittencount float64 = 0
	url := fmt.Sprintf("https://%s//api/dcim/devices/", app.nb.host)
	for {
		log.Debug("Fetching ", url)
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Set("Authorization", fmt.Sprintf("Token %s", app.nb.key))
		resp, err := client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		bodyText, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		var data NbDevices
		err = json.Unmarshal(bodyText, &data)
		if err != nil {
			panic(err)
		}

		for i := range data.Results {
			name := data.Results[i].Name

			// only process selected hosts
			if !hosts[name] {
				log.Debug("Skipping netbox host ", name)
				continue
			}
			log.Debug("Processing machted host ", name)
			delete(hosts, name)

			metricLastRunTimestamp.Set(float64(time.Now().Unix()))
			metricRunDuration.Set(float64(*flagDuration))
			metricSite.WithLabelValues(data.Results[i].Site.Slug).Set(float64(1))
			metricLocation.WithLabelValues(data.Results[i].Location.Slug).Set(float64(1))
			metricRack.WithLabelValues(data.Results[i].Rack.Name).Set(float64(data.Results[i].Position))

			if !*flagWrite {
				promfmt.Fprint(os.Stdout, promfmt.NewPrefixFilter("nbne_"))
				return
			}
			// write host-specific prom files (to be distributed to the individual hosts)
			promfile := fmt.Sprintf("%s/nbne.prom-%s", *flagOutputdir, name)
			if err := promfmt.WriteFile(promfile, promfmt.NewPrefixFilter("nbne_")); err == nil {
				promwrittencount++
			} else {
				log.Fatalf("Failed to write to prom file: %s", err)
			}
			metricRack.Reset()
			metricSite.Reset()
			metricLocation.Reset()
		}
		if data.Next == "" {
			log.Debug("No next page")
			break
		}
		url = data.Next
	}

	// same thing for vm's (lelijk)
	url = fmt.Sprintf("https://%s//api/virtualization/virtual-machines/", app.nb.host)
	for {
		log.Debug("Fetching ", url)
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Set("Authorization", fmt.Sprintf("Token %s", app.nb.key))
		resp, err := client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		bodyText, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		var data NbVirtualmachines
		err = json.Unmarshal(bodyText, &data)
		if err != nil {
			panic(err)
		}

		for i := range data.Results {
			name := data.Results[i].Name

			// only process selected hosts
			if !hosts[name] {
				log.Debug("Skipping netbox host ", name)
				continue
			}
			log.Debug("Processing machted host ", name)
			delete(hosts, name)

			metricLastRunTimestamp.Set(float64(time.Now().Unix()))
			metricRunDuration.Set(float64(*flagDuration))
			metricCluster.WithLabelValues(data.Results[i].Cluster.Name).Set(float64(1))

			if !*flagWrite {
				promfmt.Fprint(os.Stdout, promfmt.NewPrefixFilter("nbne_"))
				return
			}
			// write host-specific prom files (to be distributed to the individual hosts)
			promfile := fmt.Sprintf("%s/nbne.prom-%s", *flagOutputdir, name)
			if err := promfmt.WriteFile(promfile, promfmt.NewPrefixFilter("nbne_")); err == nil {
				promwrittencount++
			} else {
				log.Fatalf("Failed to write to prom file: %s", err)
			}
			metricCluster.Reset()
		}
		if data.Next == "" {
			log.Debug("No next page")
			break
		}
		url = data.Next
	}
	for host := range hosts {
		log.Debugf("Host not found in netbox: %q", host)
	}

	// metametrics
	metricMetaLastRunTimestamp.Set(float64(time.Now().Unix()))
	metricMetaRunDuration.Set(float64(*flagDuration))

	// number of prom file written
	metricMetaPromWrittenCount.Set(promwrittencount)

	// number of missing prom files (specified but not found in netbox)
	metricMetaPromMissing.Set(float64(len(hosts)))

	if *flagWrite {
		// use direct path here
		if err := promfmt.WriteFile(*flagPromMetaPath, promfmt.NewPrefixFilter("nbnemeta_")); err != nil {
			log.Fatalf("Failed to write to prom file: %s", err)
		}
	} else {
		promfmt.Fprint(os.Stdout, promfmt.NewPrefixFilter("nbnemeta_"))
	}
}

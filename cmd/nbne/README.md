# nbne NetBox Nodeexporter Exporter

Schrijft een `nbne.prom-{host}` file voor elke hostnaam gevoerd aan stdin.

Gebruik:

```
export NETBOX_HOST=netbox.example.com
export NETBOX_KEY=XX-my-secret-netbox-api-key-with-read-access-XX

cat listofhosts-each-host-on-a-line | nbne -w -o /tmp/nbne -m /tmp/nbnemeta.prom
```

A result `.prom` file looks like:
```
# HELP nbne_location Netbox location (server room name).
# TYPE nbne_location gauge
nbne_location{name="hg04-070"} 1
# HELP nbne_rack Netbox rack (value is the rack unit / height ).
# TYPE nbne_rack gauge
nbne_rack{name="114-11"} 37
# HELP nbne_run_duration_seconds Gauge of current configured loop time.
# TYPE nbne_run_duration_seconds gauge
nbne_run_duration_seconds 60
# HELP nbne_site Netbox site (building).
# TYPE nbne_site gauge
nbne_site{name="huygens-gebouw"} 1
```

With the `-m` flag, an additional meta prom-file is written that show the performance of nbne. Sample content:

```
# HELP nbnemeta_last_run_timestamp_seconds Epoch timestamp of the last run.
# TYPE nbnemeta_last_run_timestamp_seconds gauge
nbnemeta_last_run_timestamp_seconds 1.670928481e+09
# HELP nbnemeta_prom_missing_count_total Number of prom files missing.
# TYPE nbnemeta_prom_missing_count_total gauge
nbnemeta_prom_missing_count_total 1
# HELP nbnemeta_prom_requested_count_total Number of prom files written.
# TYPE nbnemeta_prom_requested_count_total gauge
nbnemeta_prom_requested_count_total 5
# HELP nbnemeta_prom_written_count_total Number of prom files written.
# TYPE nbnemeta_prom_written_count_total gauge
nbnemeta_prom_written_count_total 4
# HELP nbnemeta_run_duration_seconds Gauge of current configured loop time.
# TYPE nbnemeta_run_duration_seconds gauge
nbnemeta_run_duration_seconds 60
```

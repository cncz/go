package main

import (
	"os"
	"testing"
)

func TestParse(t *testing.T) {
	buffer, err := os.ReadFile("testdata/dhcp.conf")
	if err != nil {
		t.Fatal(err)
	}
	ethernets, err := parse(string(buffer), "testdata/dhcp.conf")
	if err != nil {
		t.Fatal(err)
	}
	if len(ethernets) != 9 {
		t.Errorf("expected 9 entries, got %d", len(ethernets))
	}
	eth0 := ethernets[0]
	if eth0.mac != "00:0c:df:03:08:99" {
		t.Errorf("expected mac %s, got %s", "00:0c:df:03:08:99", eth0.mac)
	}
	if eth0.name != "jaicam24.fxmnet.science.ru.nl" {
		t.Errorf("expected name %s, got %s", "jaicam24.fxmnet.science.ru.nl", eth0.name)
	}
}

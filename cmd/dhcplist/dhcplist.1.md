%%%
title = "dhcplist 1"
area = "User Commands"
workgroup = "C&CZ"
%%%

dhcplist
=====

## Name

dhcplist - parse ISC DHCP config and output host and ethernet or lookup MAC addresses

## Synopsis

dhcplist `*[OPTION]*...` `*FILE...*`

## Description

Dhcplist parses each ISC DHCP config **FILE** and outputs:
~~~
pdu-kast114-36-pdu3.net.science.ru.nl 28:29:86:45:ee:02
~~~

for each host and MAC address found. Optionally the output format can be a JSON. This output is
mainly used for the `mwol` utility.

If the `-l` option is given, it will read one host per line from standard input and will print the
hostname and the mac address found to standard output. Echo host will be looked up in the (online)
JSON list. If nothing is found an error is printed.

Options:

`-d`
:   strip domain portion from names

`-j`
:   output JSON

## Author

Miek Gieben <miek@miek.nl>.

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/alecthomas/chroma/v2"
	"github.com/alecthomas/chroma/v2/lexers"
)

var (
	flagJSON = flag.Bool("j", false, "output JSON")
	flagDrop = flag.Bool("d", false, "drop domain from each name")
)

type Ethernet struct {
	mac  string
	name string
}

func main() {
	flag.Parse()

	ethernets := []Ethernet{}
	for _, a := range flag.Args() {
		dhcp, err := os.ReadFile(a)
		if err != nil {
			log.Fatal(err)
		}

		eths, err := parse(string(dhcp), a)
		if err != nil {
			log.Fatal(err)
		}
		ethernets = append(ethernets, eths...)
	}

	if len(ethernets) == 0 {
		return
	}

	if !*flagJSON {
		for _, e := range ethernets {
			fmt.Printf("%s %s\n", e.name, e.mac)
		}
		return
	}

	m := map[string]string{}
	for _, e := range ethernets {
		m[e.name] = e.mac
	}

	out, err := json.MarshalIndent(m, " ", " ")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", out)
}

func parse(buffer, name string) ([]Ethernet, error) {
	lexer := lexers.Get("iscdhcpd")
	iter, err := lexer.Tokenise(nil, buffer)
	if err != nil {
		return nil, err
	}
	// extract the following and then just print
	// <ethernet> <space> <name>
	/*
	   chroma.Token {KeywordType host}
	   chroma.Token {Text  }
	   chroma.Token {NameVariable pdu-kast73-3-pdu8}
	   chroma.Token {Text    }
	   chroma.Token {Punctuation {}
	   chroma.Token {Text  }
	   chroma.Token {Keyword hardware}
	   chroma.Token {Text  }
	   chroma.Token {Keyword ethernet}
	   chroma.Token {Text  }
	   chroma.Token {LiteralNumberHex 00:16:18:76:00:d1}
	   chroma.Token {Punctuation ;}
	   chroma.Token {Text  }
	   chroma.Token {Keyword fixed-address}
	   chroma.Token {Text  }
	   chroma.Token {NameVariable pdu-kast73-3-pdu8.net.science.ru.nl}
	   chroma.Token {Punctuation ;}
	*/

	list := []Ethernet{}
	e := Ethernet{}
	inhost := 0
	for _, t := range iter.Tokens() {
		// debugfmt.Printf("%T %v\n", t, t)
		if t.Type == chroma.KeywordType && t.Value == "host" {
			inhost = 1
			continue
		}

		if inhost == 1 && t.Type == chroma.Keyword && t.Value == "fixed-address" {
			inhost = 2
			continue
		}

		// The tokenizer isn't perfect yet (MR going to be upstreamed).
		// so host-decl-name is wrong seen as a host keyword.
		// ignore errors for now. TODO(miek): 2023-03-27

		if inhost == 2 && t.Type == chroma.Punctuation && t.Value == ";" {
			if e.mac == "" || e.name == "" {
				e.name = ""
				e.mac = ""
				inhost = 0
				continue
			}
			list = append(list, e)
			inhost = 0
			e.mac, e.name = "", ""
			continue
		}

		if inhost == 1 && t.Type == chroma.LiteralNumberHex {
			e.mac = t.Value
		}

		if inhost == 2 && t.Type == chroma.NameVariable {
			e.name = t.Value
			if *flagDrop {
				firstDot := strings.Index(t.Value, ".")
				if firstDot > 0 {
					e.name = t.Value[:firstDot]
				}
			}
		}
	}
	return list, nil
}

# Promconf

Genereer prometheus configuratie. Deze tool werkt als een filter en converteerd de input in een
prometheus.yaml file.

## Input

De input ziet er zo uit:

~~~ txt
metrics_path: <path>  # optional
target: <host>:<port>
label: A  # optional labels for target
label: B
job_name: <job>
~~~

Elke nieuwe job_name genereert een nieuw sectie in de config met een setje static_configs targets.

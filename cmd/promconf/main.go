package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"gopkg.in/yaml.v2"
)

type ScrapeConfig struct {
	Jobs []Job `yaml:"scrape_config"`
}

type Job struct {
	Name         string `yaml:"job_name"`
	StaticConfig `yaml:"static_configs"`
}

type StaticConfig struct {
	MetricsPath string `yaml:"metrics_path,omitempty"`
	Targets     []string
	Labels      map[string]string `yaml:"labels,omitempty"`
}

func GenerateConfig(r io.Reader) ([]byte, error) {
	scanner := bufio.NewScanner(r)
	c := &ScrapeConfig{}
	j := Job{}
	for scanner.Scan() {
		in := scanner.Text()
		switch {
		case strings.HasPrefix(in, "job_name: "):
			j.Name = parameter(in)
			c.Jobs = append(c.Jobs, j)

			j = Job{}

		case strings.HasPrefix(in, "metrics_path: "):
			j.StaticConfig.MetricsPath = parameter(in)

		case strings.HasPrefix(in, "target: "):
			j.StaticConfig.Targets = append(j.StaticConfig.Targets, parameter(in))

		case strings.HasPrefix(in, "label: "):
			param := parameter(in)
			values := strings.Split(param, ":")
			if len(values) != 2 {
				log.Printf("Got %q, but does not have valid label", in)
				continue
			}
			if j.StaticConfig.Labels == nil {
				j.StaticConfig.Labels = map[string]string{}
			}
			j.StaticConfig.Labels[values[0]] = strings.TrimSpace(values[1])
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return yaml.Marshal(c)
}

// get the parameter from input.
func parameter(s string) string {
	colon := strings.Index(s, ":")
	param := s[colon+1:]
	return strings.TrimSpace(param)
}

func main() {
	data, err := GenerateConfig(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print(string(data))
}

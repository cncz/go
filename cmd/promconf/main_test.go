package main

import (
	"strings"
	"testing"
)

func TestGenerateConfig(t *testing.T) {
	in := `target: localhost:9000
target: localhost:9001
label: env: production
job_name: bliep
target: localhost:3000
job_name: blaap
`

	expected := `scrape_config:
- job_name: bliep
  static_configs:
    targets:
    - localhost:9000
    - localhost:9001
    labels:
      env: production
- job_name: blaap
  static_configs:
    targets:
    - localhost:3000
`

	r := strings.NewReader(in)
	data, err := GenerateConfig(r)
	if err != nil {
		t.Fatal(err)

	}

	if string(data) != expected {
		t.Fatalf("expected %s, got %s", expected, string(data))
	}
}

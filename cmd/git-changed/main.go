package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
)

var (
	flagEnv   = flag.String("e", "ACCESS_TOKEN", "environment variable to use for oauth token")
	flagDry   = flag.Bool("n", false, "perform a dry-run")
	flagDebug = flag.Bool("d", false, "debug; echo commands")
)

func main() {
	flag.Parse()
	commitBranch := os.Getenv("CI_COMMIT_BRANCH")
	defaultBranch := os.Getenv("CI_DEFAULT_BRANCH")

	if commitBranch == "" {
		log.Fatal("CI_COMMIT_BRANCH is not set, exiting")
	}
	if defaultBranch == "" {
		log.Fatal("CI_DEFAULT_BRANCH is not set, exiting")
	}

	switch flag.Arg(0) {
	case "setup":
		if err := setupCIBot(defaultBranch); err != nil {
			log.Fatal(err)
		}
	case "":
		// We are in another branch and the pipeline is running, this can only mean we're running a pipeline for a
		// merge request.
		if commitBranch != defaultBranch {
			if err := mergeRequestDiff(defaultBranch); err != nil {
				log.Fatal(err)
			}
			if err := modifiedDiff("AM"); err != nil {
				log.Fatal(err)
			}
			return
		}

		// The branch is the default branch, use CI_COMMIT_BEFORE_SHA to figure out what has changed.
		beforeSHA := os.Getenv("CI_COMMIT_BEFORE_SHA")
		if beforeSHA == "" {
			log.Fatal("CI_COMMIT_BEFORE_SHA is not set, exiting")
		}

		if err := defaultDiff(beforeSHA); err != nil {
			log.Fatal(err)
		}
		if err := modifiedDiff("AM"); err != nil {
			log.Fatal(err)
		}
	default:
		log.Fatalf("Unknown verb %q", flag.Arg(0))
	}
}

func defaultDiff(sha string) error {
	diff, err := git("diff-tree", "--name-only", "-r", "HEAD", sha)
	if err != nil {
		return fmt.Errorf("failed to run git diff-tree --name-only -r HEAD %s: %s", sha, err)
	}
	fmt.Printf("%s\n", diff)
	return nil
}

func mergeRequestDiff(branch string) error {
	_, err := git("fetch", "origin", branch)
	if err != nil {
		return fmt.Errorf("failed to run git fetch origin %s: %s", branch, err)
	}

	diff, err := git("diff-tree", "--name-only", "-r", fmt.Sprintf("HEAD..origin/%s", branch))
	if err != nil {
		return fmt.Errorf("failed to run git diff-tree --name-only -r HEAD..origin/%s: %s", branch, err)
	}
	fmt.Printf("%s\n", diff)
	return nil
}

func modifiedDiff(filter string) error {
	diff, err := git("diff", "--name-only", fmt.Sprintf("--diff-filter=%s", filter))
	if err != nil {
		return fmt.Errorf("failed to run git diff --name-only --diff-filter=%s", filter)
	}
	fmt.Printf("%s\n", diff)
	return nil
}

func setupCIBot(branch string) error {
	serverHost := os.Getenv("CI_SERVER_HOST")
	if serverHost == "" {
		return fmt.Errorf("CI_SERVER_HOST is not set")
	}
	projectName := os.Getenv("CI_PROJECT_NAME")
	if projectName == "" {
		return fmt.Errorf("CI_PROJECT_NAME is not set")
	}
	projectNamespace := os.Getenv("CI_PROJECT_NAMESPACE")
	if projectNamespace == "" {
		return fmt.Errorf("CI_PROJECT_NAMESPACE is not set")
	}
	accessToken := os.Getenv(*flagEnv)
	if accessToken == "" {
		return fmt.Errorf("%s is not set", *flagEnv)
	}
	if _, err := git("config", "user.name", "ci-bot"); err != nil {
		return err
	}
	if _, err := git("config", "user.email", fmt.Sprintf("%s@%s", "ci-bot", serverHost)); err != nil {
		return err
	}
	if _, err := git("remote", "remove", "gitlab_origin"); err != nil {
		log.Printf("Failed to remove gitlab_origin (not-fatal): %s", err)
	}
	if _, err := git("remote", "add", "gitlab_origin", fmt.Sprintf("https://oauth2:%s@%s/%s/%s", accessToken, serverHost, projectNamespace, projectName)); err != nil {
		log.Printf("Failed to add gitlab_origin (not-fatal): %s", err)
	}
	if _, err := git("pull", "origin", branch); err != nil {
		return err
	}
	return nil
}

func git(args ...string) ([]byte, error) {
	git := exec.Command("git", args...)
	if *flagDry {
		log.Printf("%s", git)
		return nil, nil
	}
	if *flagDebug {
		log.Printf("%s", git)
	}
	return git.Output()
}

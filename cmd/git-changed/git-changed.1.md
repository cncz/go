%%%
title = "git-changed 1"
area = "Git Manual"
workgroup = "C&CZ"
%%%

# git-changed

## Name

git-changed - list changed files in a GitLab pipeline

## Synopsis

git-changed [**OPTIONS**] [setup]

## Description

Git-changed lists what files are changed in the current merge request, or when a straight push to
the default branch (usually `main` or `master`) is done. Locally modified files are included in this
list via `git diff --name-only --diff-filter=AM`.

When in a merge request the `$CI_DEFAULT_BRANCH` is fetched and a `git diff-tree name-only` is
performed against it.

For straight pushes to `main` or `master` it will use `$CI_COMMIT_BEFORE_SHA` and run a `diff-tree`
with HEAD and that SHA value.

The exit status is non zero if there was an error running Git are the environment is not correct.

If *setup* is given Git is configured:

~~~ sh
- git config user.email "ci-bot@$CI_SERVER_HOST"
- git config user.name "ci-bot"
- git remote remove gitlab_origin || true
- git remote add gitlab_origin https://oauth2:$ACCESS_TOKEN@$CI_PROJECT_URL || true
- git pull origin $CI_DEFAULT_BRANCH
~~~

## Options

`-e` *ENV*
:  use the environment variables named *ENV* as the ACCESS_TOKEN in the following command line:
   `git remote add gitlab_origin https://oauth2:$ACCESS_TOKEN@$CI_PROJECT_URL`.
   The default is to use `ACCESS_TOKEN`.

`-n`
:  dry run, only echo commands.

`-d`
:  debug, echo commands and run them.

## Environment

This command needs to run in the GitLab (runner) environment, and accesses the following environment
variables:

* `CI_COMMIT_BRANCH`
* `CI_DEFAULT_BRANCH`
* `CI_COMMIT_BEFORE_SHA`
* `CI_PROJECT_NAME` (for `setup`)
* `CI_PROJECT_NAMESPACE` (for `setup`)
* `CI_SERVER_HOST` (for `setup`)

## Notes

This could have been a shell script as it just wraps Git commands, but creating a Go program makes
it easier to integrate it in our Debian-package deployment and allows for Go unit testing.

## See Also

See this list of [pre-defined
variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) in GitLab.

## Author

Miek Gieben <miek@miek.nl>

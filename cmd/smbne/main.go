package main

import (
	"flag"
	"os"
	"strings"
	"time"

	"go.science.ru.nl/log"
	"go.science.ru.nl/promfmt"
)

var (
	flagWrite    = flag.Bool("w", true, "write to /var/lib/prometheus/node-exporter/smbne.prom")
	flagDuration = flag.Uint("t", 60, "default duration to export in seconds")
	flagDebug    = flag.Bool("d", false, "enable debug logging")
)

const promfile = "/var/lib/prometheus/node-exporter/smbne.prom"

func main() {
	flag.Parse()
	if *flagDebug {
		log.D.Set()
	}
	doit()
}

func doit() {
	out, err := smbStatus()
	if err != nil {
		// don't update metrics so timestamp will age.
		log.Warningf("Failed to run fail2ban status")
		return
	}
	ls := smbStatusLock(out)
	log.Infof("Getting smbstatus data for %d locks", len(ls))
	for k, v := range ls {
		fields := strings.Split(k, ",")
		uid := fields[0]
		share := fields[1]
		metricLockTotal.WithLabelValues(share, uid).Set(float64(v))
	}
	metricLastRunTimestamp.Set(float64(time.Now().Unix()))
	metricRunDuration.Set(float64(*flagDuration))

	if !*flagWrite {
		promfmt.Fprint(os.Stdout, promfmt.NewPrefixFilter("smbne_"))
		return
	}

	if err := promfmt.WriteFile(promfile, promfmt.NewPrefixFilter("smbne_")); err != nil {
		log.Fatalf("Failed to write to prom file: %s", err)
	}
}

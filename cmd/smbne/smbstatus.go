package main

import (
	"bufio"
	"bytes"
	"os/exec"
	"strings"
)

// map of uid,share -> #locks
type lock map[string]int

func smbStatus() ([]byte, error) {
	cmd := exec.Command("smbstatus", "-L", "-n")
	out, err := cmd.Output()
	if err != nil {
		return out, err
	}
	res := bytes.TrimSpace(out)
	return res, nil
}

func smbStatusLock(data []byte) lock {
	scanner := bufio.NewScanner(bytes.NewReader(data))
	ls := lock{}
	for scanner.Scan() {
		fields := strings.Fields(scanner.Text())
		// spaces are shown latter in the path, the fields we care about are space seperated
		if len(fields) < 7 {
			continue
		}
		if fields[0] == "Pid" {
			continue
		}

		share := fields[6]
		uid := fields[1]
		ls[uid+","+share] += 1
	}
	return ls
}

smbne

Go program that exports samba lock data to prometheus node exporter.

The following metrics are dumped:

- `smbne_last_run_timestamp_seconds <epoch>` (gauge)
- `smbne_run_duration_seconds <duration>` (gauge)
- `smbne_lock_count_total{share="<export/share>",uid="<uid">} <num>` (gauge)
  This is a dump of smbstatus -L -n.
  ~~~
  Pid          User(ID)   DenyMode   Access      R/W        Oplock           SharePath   Name   Time
  --------------------------------------------------------------------------------------------------
  1232708      **939      DENY_NONE  0x100081    RDONLY     NONE             /export/owc   x   Wed Mar  6 11:41:30 2024
  1232708      **939      DENY_NONE  0x100081    RDONLY     NONE             /export/owc   x  Wed Mar  6 11:41:30 2024
  ~~~

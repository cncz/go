package main

import (
	"testing"
)

const locklist = `Pid          User(ID)   DenyMode   Access      R/W        Oplock           SharePath   Name   Time
--------------------------------------------------------------------------------------------------
1232708      **939      DENY_NONE  0x100081    RDONLY     NONE             /export/owc   x   Wed Mar  6 11:41:30 2024
1232708      **939      DENY_NONE  0x100081    RDONLY     NONE             /export/owc   x  Wed Mar  6 11:41:30 2024
`

func TestParseLock(t *testing.T) {
	locks := smbStatusLock([]byte(locklist))
	if len(locks) != 1 {
		t.Errorf("expected 1 lock, got %d", len(locks))
	}
	v, ok := locks["**939,/export/owc"]
	if !ok {
		t.Errorf("expected value for %s", "**939,/export/owc")
	}
	if v != 2 {
		t.Errorf("expected value %d, got %d", 2, v)
	}
}

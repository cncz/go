package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/jpillora/opts"
	"github.com/microcosm-cc/bluemonday"
	"github.com/yosssi/gohtml"
)

func main() {
	type config struct {
		Elements   []string `opts:"help=allowed html elements"`
		Attributes []string `opts:"help=allowed element attributes ie: td.colspan"`
	}
	c := config{}
	opts.New(&c).
		Summary(`
Strict filter html and allow only specified elements and attributes.
When allowing attributes on an element, the element is implicitly allowed too.
Ie. when allowing 'a.href', the 'a'-element is allowe too and need not be specified as
an allowd element.
Depending on the allowed html-elements, the output might be valid HTML.

example: 

 hstrip -e a,table,tr,th,td -a a.href < test.html
  
`).
		Parse()

	// Define a policy, we are using the Strict policy as a base.
	p := bluemonday.StrictPolicy()

	// allow attributes
	for _, value := range c.Attributes {
		for _, value1 := range strings.Split(value, ",") {
			if !strings.Contains(value1, ".") {
				log.Fatal("no dot fount, please specify attribute as element.attribute")
			}
			parts := strings.Split(value1, ".")
			if len(parts) != 2 {
				log.Fatalf("attribute '%s' should be formatted as element.attribute", value1)
			}
			p.AllowAttrs(parts[1]).OnElements(parts[0])
		}
	}

	// allow elements
	for _, value := range strings.Split(strings.Join(c.Elements[:], ","), ",") {
		p.AllowElements(value)
	}
	// Read input from stdin so that this is a nice unix utility and can receive
	// piped input
	dirty, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}

	// Apply the policy and write formatted html to stdout
	gohtml.Condense = true
	fmt.Fprint(os.Stdout, gohtml.Format(p.Sanitize(string(dirty))))
}

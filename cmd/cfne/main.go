package main

import (
	"bytes"
	"flag"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/icza/backscanner"
	"go.science.ru.nl/log"
	"go.science.ru.nl/promfmt"
)

var (
	flagWrite    = flag.Bool("w", false, "write files")
	flagDuration = flag.Uint("t", 3600, "default duration to export in seconds")
	flagDebug    = flag.Bool("d", false, "enable debug logging")
	flagLogfile  = flag.String("l", "/var/cfengine/promise_summary.log", "promise_summary.log file")
	flagPromFile = flag.String("p", "/var/lib/prometheus/node-exporter/cfne.prom", "prom file path for writing cfne.prom")
)

func findNamedMatches(regex *regexp.Regexp, str string) map[string]string {
	match := regex.FindStringSubmatch(str)

	results := map[string]string{}
	for i, name := range match {
		results[regex.SubexpNames()[i]] = name
	}
	return results
}

func stringToFloat64(s string) float64 {
	f, _ := strconv.ParseFloat(s, 64)
	return f
}

func ParsePromiseSummary(line string) map[string]string {
	re := regexp.MustCompile(`(?m)^(?P<start_time>\d+),(?P<end_time>\d+): Outcome of version (?P<version_string>.*?) \(.*?\): Promises observed to be kept (?P<promises_kept>\d{1,3}(\.\d\d)?)%, Promises repaired (?P<promises_repaired>\d{1,3}(\.\d\d)?)%, Promises not repaired (?P<promises_not_repaired>\d{1,3}(\.\d\d)?)%$`)
	matches := findNamedMatches(re, line)
	if len(matches) == 0 {
		log.Fatal("Could not parse promise_summary log line")
	}
	return matches
}

func GetLastMatchingLogline(buf []byte, pat string) string {
	scanner := backscanner.New(bytes.NewReader(buf), len(buf))
	for {
		line, _, err := scanner.Line()
		if err != nil {
			log.Warningf("Failed to scan backwards: %s", err)
			return ""
		}
		if strings.Contains(line, pat) {
			return line
		}
	}
}

func main() {
	flag.Parse()
	if *flagDebug {
		log.D.Set()
	}

	// log file
	_, err := os.Lstat(*flagLogfile)
	if err != nil {
		log.Fatal(err)
	}

	metricLastRunTimestamp.Set(float64(time.Now().Unix()))
	metricRunDuration.Set(float64(*flagDuration))

	buf, err := os.ReadFile(*flagLogfile)
	if err != nil {
		log.Fatalf("Failed top read file %q: %s", *flagLogfile, err)
	}

	// report various cf-files with the cf label
	for _, CfFile := range []string{"update.cf", "Promises.cf", "Failsafe.cf"} {
		str := GetLastMatchingLogline(buf, CfFile)
		if str == "" { // skip empty match, happens on new log file?
			continue
		}

		matches := ParsePromiseSummary(str)
		log.Debugf("matches: %#v", matches)

		file := strings.ToLower(CfFile)

		metricPromisesKept.WithLabelValues(file).Set(stringToFloat64(matches["promises_kept"]))
		metricPromisesRepaired.WithLabelValues(file).Set(stringToFloat64(matches["promises_repaired"]))
		metricPromisesNotRepaired.WithLabelValues(file).Set(stringToFloat64(matches["promises_not_repaired"]))
		metricLastRunSeconds.WithLabelValues(file).Set(stringToFloat64(matches["start_time"]))
		metricRunTimeSecondsTotal.WithLabelValues(file).Set(stringToFloat64(matches["end_time"]) - stringToFloat64(matches["start_time"]))

		metricVersion.WithLabelValues(file, strings.ToLower(matches["version_string"])).Set(float64(1))
	}
	if *flagWrite {
		log.Debug("About to write cfne_ prefixed metrics to ", *flagPromFile)
		if err := promfmt.WriteFile(*flagPromFile, promfmt.NewPrefixFilter("cfne_")); err != nil {
			log.Fatalf("Failed to write to prom file: %s", *flagPromFile)
		}
		return
	}

	log.Debug("Printing cfne_ prefixed metrics")
	promfmt.Fprint(os.Stdout, promfmt.NewPrefixFilter("cfne_"))
}

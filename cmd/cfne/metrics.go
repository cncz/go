package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	metricLastRunTimestamp = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "cfne_last_run_time_seconds",
		Help: "Epoch timestamp of the last run.",
	})
	metricRunDuration = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "cfne_run_duration_seconds",
		Help: "Gauge of current configured loop time.",
	})

	metricPromisesKept = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "cfne_cfengine_promises_kept",
		Help: "Percentage of promises kept during last run.",
	}, []string{"cf"})
	metricPromisesRepaired = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "cfne_cfengine_promises_repaired",
		Help: "Percentage of promises repaired during last run.",
	}, []string{"cf"})
	metricPromisesNotRepaired = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "cfne_cfengine_promises_not_repaired",
		Help: "Percentage of promises not kept during last run.",
	}, []string{"cf"})
	metricLastRunSeconds = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "cfne_cfengine_last_run_seconds",
		Help: "Seconds since epoch of last run.",
	}, []string{"cf"})
	metricRunTimeSecondsTotal = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "cfne_cfengine_run_time_seconds_total",
		Help: "TODO",
	}, []string{"cf"})
	metricVersion = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "cfne_cfengine_version_info",
		Help: "A metric with a constant '1' value labeled by version.",
	}, []string{"cf", "version"})
)

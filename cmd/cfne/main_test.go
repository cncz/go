package main

import (
	"fmt"
	"testing"
)

func TestParsePromiseSummary(t *testing.T) {
	in := `1671011660,1671011697: Outcome of version CFEngine Promises.cf  3.15.3 (agent-0): Promises observed to be kept 99.08%, Promises repaired 0.85%, Promises not repaired 0.07%`
	expected := map[string]string{"": ".07", "end_time": "1671011697", "promises_kept": "99.08", "promises_not_repaired": "0.07", "promises_repaired": "0.85", "start_time": "1671011660", "version_string": "CFEngine Promises.cf  3.15.3"}

	got := ParsePromiseSummary(in)

	// convert to something we can compare and report
	gotStr := fmt.Sprint(got)
	expectedStr := fmt.Sprint(expected)
	if gotStr != expectedStr {
		t.Fatalf("expected %s, got %s", expectedStr, gotStr)
	}
}

package main

import (
	"testing"
)

const jaillist = "Status\n" +
	"|- Number of jail:	5\n" +
	"`- Jail list:	sshd, wordpress-wplogin-http, wordpress-wplogin-https, wordpress-xmlrpc-http, wordpress-xmlrpc-https\n"

func TestParseJailList(t *testing.T) {
	jails := parseJailList([]byte(jaillist))
	if len(jails) != 5 {
		t.Errorf("expected 5 jails, got %d", len(jails))
	}
	expect := []string{"sshd", "wordpress-wplogin-http", "wordpress-wplogin-https", "wordpress-xmlrpc-http", "wordpress-xmlrpc-https"}
	for i := range jails {
		if jails[i] != expect[i] {
			t.Errorf("expected jail %q, got: %s", jails[i], expect[i])
		}
	}
}

const jailinfo = "Status for the jail: sshd\n" +
	"|- Filter\n" +
	"|  |- Currently failed:	2\n" +
	"|  |- Total failed:	3\n" +
	"|  `- File list:	/var/log/auth.log\n" +
	"`- Actions\n" +
	"   |- Currently banned:	5\n" +
	"   |- Total banned:	15\n" +
	"   `- Banned IP list:	\n"

func TestParseJail(t *testing.T) {
	j := parseJail([]byte(jailinfo), "sshd")
	if j == nil {
		t.Fatal("expected jail, got none")
	}

	if j.name != "sshd" {
		t.Errorf("expected name %q: got %s", "sshd", j.name)
	}
	if j.failedTotal != 3 {
		t.Errorf("expected failedTotal %d: got %f", 3, j.failedTotal)
	}
	if j.bannedTotal != 15 {
		t.Errorf("expected failedTotal %d: got %f", 15, j.failedTotal)
	}
	if j.bannedNow != 5 {
		t.Errorf("expected bannedNow %d: got %f", 5, j.bannedNow)
	}
	if j.failedNow != 2 {
		t.Errorf("expected bannedNow %d: got %f", 2, j.failedNow)
	}
}

f2bne

Go program that exports f2b status logs into node exporter. Metrics are dumped on standard output.

The following metrics are dumped:

- `f2bne_last_run_timestamp_seconds <epoch>` (gauge)
- `f2bne_run_duration_seconds <duration>` (gauge)
- `f2bne_failed_count_total{jail="<jail>"} <num>` (gauge)
- `f2bne_banned_count_total{jail="<jail>"} <num>` (gauge)
- `f2bne_failed_count_current{jail="<jail>"} <num>` (gauge)
- `f2bne_banned_count_current{jail="<jail>"} <num>` (gauge)
- `f2bne_banned_count_current{jail="<jail>"} <num>` (gauge)

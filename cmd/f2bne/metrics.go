package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	metricLastRunTimestamp = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "f2bne_last_run_time_seconds",
		Help: "Epoch timestamp of the last run.",
	})
	metricRunDuration = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "f2bne_run_duration_seconds",
		Help: "Gauge of current configured loop time.",
	})
	metricFailedTotal = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "f2bne_failed_count_total",
		Help: "Gauge of total failed filters.",
	}, []string{"jail"})
	metricBannedTotal = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "f2bne_banned_count_total",
		Help: "Gauge of total banned.",
	}, []string{"jail"})
	metricFailedNow = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "f2bne_failed_count_current",
		Help: "Gauge of current failed filters.",
	}, []string{"jail"})
	metricBannedNow = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "f2bne_banned_count_current",
		Help: "Gauge of current banned IPs",
	}, []string{"jail"})
)

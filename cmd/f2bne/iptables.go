package main

import (
	"context"
	"os/exec"

	"go.science.ru.nl/log"
)

func iptables(args ...string) ([]byte, error) {
	ctx := context.TODO()
	cmd := exec.CommandContext(ctx, "/sbin/iptables", args...)
	log.Infof("running iptables %v", cmd.Args)

	out, err := cmd.CombinedOutput()
	if len(out) > 0 {
		log.Debug(string(out))
	}

	return out, err
}

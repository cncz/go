package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os/exec"
)

type Config struct {
	Targets []string          `json:"targets"`
	Labels  map[string]string `json:"labels,omitempty"`
}

type Configs []*Config

func main() {
	type Label struct {
		Labelname  string
		Labelvalue string
	}
	//var hostlabels map[string][]Label
	var hosts []string

	log.Printf("Running cfgroup to find %q hosts", "HasNodeExporter")
	out, err := exec.Command("/etc/cncz/bin/cfgroup", "HasNodeExporter").Output()
	if err != nil {
		log.Fatal(err)
	}

	r := bytes.NewReader(out)
	scanner := bufio.NewScanner(r)
	i := 0
	for scanner.Scan() {
		i++
		hosts = append(hosts, scanner.Text())
	}
	log.Printf("Handled %d hosts", i)
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// build sd_config json
	c := &Config{}
	for key := range hosts {
		c.Targets = append(c.Targets, hosts[key]+".science.ru.nl:9100")
	}
	cs := &Configs{c}
	jout, err := json.MarshalIndent(cs, "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Outputing to standard output")

	fmt.Printf("%s", jout)
}

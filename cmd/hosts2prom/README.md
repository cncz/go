# hosts2prom

Hosts2prom genereerd de `node_servers.json` met de target die allemaal een node_exporter draaien.

Hij voert een `cfgroup HasNodeExporter` uit om de hosts te vinden die moeten worden toevoegd.

Je moet dan dus een `host2prom > node_server.json` uitvoeren in de juiste directory op de juiste
machine.

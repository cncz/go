package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"golang.org/x/crypto/ssh"
)

var (
	flagIdent = flag.String("i", "", "identity file to use for ssh")
	flagUser  = flag.String("u", "", "user to use")
	flagPort  = flag.Uint("p", 22, "port to use")
	flagFs    = flag.String("f", "nfs", "3-letter abbreviation of filessytem to look for")
)

type Mount struct {
	Machine    string
	Root       string
	Mountpoint string
}

type Mounts struct {
	m []Mount
	sync.RWMutex
}

func (ms *Mounts) Append(m ...Mount) {
	ms.Lock()
	defer ms.Unlock()
	ms.m = append(ms.m, m...)
}

func (ms *Mounts) String() string {
	ms.RLock()
	defer ms.RUnlock()

	b := &bytes.Buffer{}
	for _, m := range ms.m {
		fmt.Fprintf(b, "%s %s %s\n", m.Machine, m.Root, m.Mountpoint)
	}

	return b.String()
}

func main() {
	flag.Parse()

	if *flagIdent == "" {
		log.Fatal("identity file not given, -i flag")
	}
	key, err := ioutil.ReadFile(*flagIdent)
	if err != nil {
		log.Fatal(err)
	}

	// Create the Signer for this private key.
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatal(err)
	}

	machines := machinesFromStdin()
	if len(machines) == 0 {
		log.Fatal("no machines given on standard input")
	}

	wg := sync.WaitGroup{}
	mounts := &Mounts{}
	for i := range machines {
		wg.Add(1)
		go func(machine string) {
			defer wg.Done()
			log.Printf("Looking at machine: %s", machine)
			buf, err := mountsWithSSH(machine, signer)
			if err != nil {
				log.Printf("machine %s, failed: %s", machine, err)
				return
			}
			ms, err := parseMountInfo(buf, machine)
			if err != nil {
				log.Printf("machine %s, failed: %s", machine, err)
			}
			mounts.Append(ms...)
		}(machines[i])
	}
	wg.Wait()
	log.Printf("DONE with %d machines", len(machines))

	fmt.Printf("%s", mounts.String())
}

func machinesFromStdin() []string {
	buf, err := io.ReadAll(os.Stdin)
	if err != nil {
		return nil
	}
	machines := bytes.Fields(buf)
	m := make([]string, len(machines))
	p := fmt.Sprintf(":%d", *flagPort)
	for i := 0; i < len(machines); i++ {
		m[i] = string(machines[i]) + p
	}
	return m
}

// query remote machine with SSH and get the mounts.
func mountsWithSSH(machine string, signer ssh.Signer) ([]byte, error) {
	config := &ssh.ClientConfig{
		User:            *flagUser,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         5 * time.Second,
	}

	client, err := ssh.Dial("tcp", machine, config)
	if err != nil {
		return nil, err
	}
	defer client.Close()
	ss, err := client.NewSession()
	if err != nil {
		return nil, err
	}
	defer ss.Close()

	stdoutBuf := &bytes.Buffer{}
	ss.Stdout = stdoutBuf

	cmdline := fmt.Sprintf(`awk 'substr($3, 1, 3) == "%s" { print $0 }' /proc/1/mounts`, *flagFs)

	if err := ss.Run(cmdline); err != nil {
		return nil, err
	}
	return stdoutBuf.Bytes(), nil
}

func parseMountInfo(buf []byte, machine string) ([]Mount, error) {
	// need to do it ourselves, simple scanner and split
	scanner := bufio.NewScanner(bytes.NewReader(buf))
	m := []Mount{}
	for scanner.Scan() {
		fields := strings.Fields(scanner.Text())
		if len(fields) < 3 {
			continue // some wrong, error??
		}
		m = append(m, Mount{
			Machine:    machine,
			Root:       fields[0],
			Mountpoint: fields[1],
		})
	}

	if err := scanner.Err(); err != nil {
		return m, err
	}
	return m, nil
}

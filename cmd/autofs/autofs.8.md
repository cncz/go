%%%
title = "autofs 8"
area = "System Administration Commands"
workgroup = "C&CZ"
%%%

autofs
=====

## Name

autofs - grab nfs mountpoints from remote servers.

## Synopsis

autofs `*[OPTION]*...`

## Description

Autofs checks the list of servers, given on standard input, for NFS/NFS4 (the default) (auto
mounted) mounts. It does this be ssh-ing to the remote servers and running an AWK command. All
server are queried in parallel. Each ssh session as a 5 second timeout on dialing the remote server,
so max run-time of this command is 5s.

Afterwards it returns a list of `<machine> <root> <mountpoint>`:

~~~
home1.science.ru.nl:/VGsda47/xxxx /home/xxxx
~~~

`-i`
:   SSH identity to use

`-p`
:   port to use (default: 22)

`-u`
:   user to use (no default)

`-f`
:   3-letter string to grep, defaults to 'nfs'

## Author

Miek Gieben <miek@miek.nl>.

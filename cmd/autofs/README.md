# autofs

Autofs checks the list of servers, given on standard input, for NFS/NFS4 (the default) automounted
mounts. It does this be ssh-ing to the remote servers and running an AWK command.

~~~
home1.science.ru.nl:/VGsda47/xxxx /home/xxxx
~~~

Would be returned by this command. All server are queried in parallel. Each ssh session as a 5
second timeout on dialing the remote server, so max run-time of this command is 5s.

When all queries are done the data is collected and printed to standard output, so the normal Unix
tooling can further massage it.

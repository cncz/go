package main

import (
	"bufio"
	"bytes"
	"context"
	"flag"
	"os"
	"os/exec"
	"strings"
	"time"

	"go.science.ru.nl/log"
	"go.science.ru.nl/promfmt"
)

var (
	flagWrite    = flag.Bool("w", true, "write to /var/lib/prometheus/node-exporter/dpkg2ne.prom")
	flagDuration = flag.Uint("t", 60, "default duration to export in seconds")
	flagDebug    = flag.Bool("d", false, "enable debug logging")
)

const promfile = "/var/lib/prometheus/node-exporter/dpkg2ne.prom"

func main() {
	flag.Parse()
	if *flagDebug {
		log.D.Set()
	}
	doit()
}

func dpkg(args ...string) ([]byte, error) {
	ctx := context.TODO()
	cmd := exec.CommandContext(ctx, "dpkg-query", args...)
	log.Debugf("running dpkg-query %v", cmd.Args)

	out, err := cmd.CombinedOutput()
	if len(out) > 0 {
		log.Debug(string(out))
	}

	return out, err
}

// zsh,5.8.1-1
// pkg,version
func parseDpkg(data []byte) error {
	scanner := bufio.NewScanner(bytes.NewReader(data))
	for scanner.Scan() {
		text := strings.ToLower(scanner.Text()) // if for some reason we have pkg with upper case in the name
		pieces := strings.Split(text, ",")
		if len(pieces) != 2 {
			continue
		}
		metricPkgVersion.WithLabelValues(pieces[0], pieces[1]).Set(1)
	}

	if scanner.Err() != nil {
		return scanner.Err()
	}

	return nil
}

func doit() {
	out, err := dpkg("-Wf", "${Package},${Version}\n")
	if err != nil {
		log.Warningf("Failed to run dpkg: %s", err)
		return
	}

	if err := parseDpkg(out); err != nil {
		log.Warningf("Failed to parse dpkg's output: %s", err)
	}

	metricLastRunTimestamp.Set(float64(time.Now().Unix()))
	metricRunDuration.Set(float64(*flagDuration))

	if !*flagWrite {
		promfmt.Fprint(os.Stdout, promfmt.NewPrefixFilter("dpkg2ne_"))
		return
	}

	if err := promfmt.WriteFile(promfile, promfmt.NewPrefixFilter("dpkg2ne_")); err != nil {
		log.Fatalf("Failed to write to prom file: %s", err)
	}
}

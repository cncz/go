# dpkg2ne

dpkg2ne exports all package versions to prometheus.

- `dpkg2ne_last_run_timestamp_seconds <epoch>` (gauge)
- `dpkg2ne_run_duration_seconds <duration>` (gauge)
- `dpkg2ne_pkg_version{package="<pkg>", version="<version>"}` (gauge)

It's essentially a small (Go) wrapper around: `dpkg-query -Wf '${Package},${Version}\n'`

Yes, this seems like to have quite some label cardinality, but we want to need this information in
Grafana. Maybe doing it via logging and Graylog at some point proves a better idea.

Compile and test run with `./dpkg2ne -w=false`.

As all *ne cmds this depends on the node-exporter text export.

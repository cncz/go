package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	metricLastRunTimestamp = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "dpkg2ne_last_run_time_seconds",
		Help: "Epoch timestamp of the last run.",
	})
	metricRunDuration = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "dpkg2ne_run_duration_seconds",
		Help: "Gauge of current configured loop time.",
	})
	metricPkgVersion = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "dpkg2ne_pkg_version",
		Help: "Gauge of package names and versions.",
	}, []string{"package", "version"})
)

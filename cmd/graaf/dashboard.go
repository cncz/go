package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/prometheus/prometheus/promql/parser"
	"go.science.ru.nl/cmd/graaf/sdk"
)

// Dashboard is the specification of the YAML dashboard.
type Dashboard struct {
	Title      string   `yaml:"title"`
	UID        string   `yaml:"uid"`
	Datasource string   `yaml:"datasource"`
	Refresh    string   `yaml:"refresh"` // Refresh value Go time.Duration syntax
	Vars       []Var    `yaml:"vars"`
	Panels     []Panel  `yaml:"panels"`
	Tags       []string `yaml:"tags"`
	Links      []Link   `yaml:"links"`
	Editable   *bool    `yaml:"editable"`
	Tooltip    string   `yaml:"tooltip"`
}

func (b *Dashboard) IsValid() error {
	if b.Title == "" {
		return fmt.Errorf("dashboard should have a title")
	}
	if b.UID == "" {
		return fmt.Errorf("dashboard should have a UID string that uniquely identifies it")
	}
	return nil
}

// Link is a link in a YAML dashboard.
type Link struct {
	Title    string   `yaml:"title"`
	URL      string   `yaml:"url"`      // set when pointing to URL
	Tags     []string `yaml:"tags"`     // set when pointing to dashboard(s)
	External bool     `yaml:"external"` // set to true when pointing to external URL
}

// Var is a variable in a YAML dashboard.
type Var struct {
	Name  string `yaml:"name"`
	Expr  string `yaml:"expr"`
	Regex string `yaml:"regex"` // .*job=\"(?<value>.*)\".* (?<text>) is the other capture group
	Multi bool   `yaml:"multi"`
	Sort  string `yaml:"sort"`
}

func (v Var) ParsePromQL() error {
	// These can be:
	// label_names(), label_values(label), label_values(metric, label), metrics(metric), or
	// query_result(query)
	//
	// Where query_result is the most powerful.

	if strings.HasPrefix(v.Expr, "label_names()") {
		return nil
	}
	if strings.HasPrefix(v.Expr, "label_values(") {
		return nil
	}

	toCheck := v.Expr
	if strings.HasPrefix(toCheck, "metrics(") {
		toCheck = toCheck[len("metrics("):]
		toCheck = toCheck[:len(toCheck)-1]
		return parsePromQL(toCheck)
	}

	if strings.HasPrefix(toCheck, "query_result(") {
		toCheck = toCheck[len("query_result("):]
		toCheck = toCheck[:len(toCheck)-1]
		return parsePromQL(toCheck)
	}

	return parsePromQL(v.Expr)
}

// Expr holds each prometheus promQL.
type Expr struct {
	Expr      string   `yaml:"expr"` // Expr is the PromQL.
	Legend    string   `yaml:"legend"`
	Transform string   `yaml:"transform"`
	Allow     []string `yaml:"allow"`               // list of things allowed, currently only value can be 'topk'.
	Threshold string   `yaml:"threshold,omitempty"` // define a threshold for this panel.
}

func (e Expr) ParsePromQL() error { return parsePromQL(e.Expr, e.Allow...) }

// Parse parses e.Expr to check if it's valid promQL, when invalid an error is returned. A semantic check is make to see
// if the job label is specified.
func parsePromQL(e string, allowed ...string) error {
	// if just a single number, skip the checks
	if _, err := strconv.ParseFloat(e, 64); err == nil {
		return nil
	}

	pq, err := parser.ParseExpr(e)
	if err != nil {
		return err
	}
	allowaggr := false
	for i := range allowed {
		if allowed[i] == "topk" || allowed[i] == "bottomk" {
			allowaggr = true
		}
	}

	// topk expressions are very expensive as they retrieve "all" metrics and then do the topk calculation. It's
	// only practical if done with a rate over a modest time period (<24h ?).
	aggr := ""
	parser.Inspect(pq, func(node parser.Node, _ []parser.Node) error {
		if as, ok := node.(*parser.AggregateExpr); ok {
			if as.Op == parser.TOPK || as.Op == parser.BOTTOMK {
				aggr = as.Op.String()
				return fmt.Errorf("found an aggr")
			}
		}
		return nil
	})

	if aggr != "" && !allowaggr {
		call := false
		// a func must be called otherwise the topk will be to expensive.
		parser.Inspect(pq, func(node parser.Node, _ []parser.Node) error {
			if _, ok := node.(*parser.Call); ok {
				call = true
				return fmt.Errorf("found a call")
			}
			return nil
		})
		if !call {
			return fmt.Errorf("expression %q has %s without anything to limit the amount of metrics returned. This OOMs prometheus", e, aggr)
		}
	}

	labels := parser.ExtractSelectors(pq)
	job := false
	for _, label := range labels {
		for _, matcher := range label {
			if matcher.Name == "job" {
				job = true
			}
		}
	}
	if !job {
		return fmt.Errorf("expression %q does not have a job matcher", e)
	}
	return nil
}

// Panel defines the panel type we support in the YAML dashboard.
type Panel struct {
	*Timeseries `yaml:"timeseries"`
	*Stat       `yaml:"stat"`
	*Heatmap    `yaml:"heatmap"`
	*News       `yaml:"news"`
	*List       `yaml:"list"`
}

// ToPanel converts a defined panel inside p to an sdk.Panel.
func (p Panel) ToPanel() (s *sdk.Panel, err error) {
	exprs := []Expr{}
	done := false
	if p.Timeseries != nil {
		s, err = p.Timeseries.ToPanel()
		exprs = p.Timeseries.Exprs
		done = true
	}
	if p.Stat != nil {
		s, err = p.Stat.ToPanel()
		exprs = p.Stat.Exprs
		done = true
	}
	if p.Heatmap != nil {
		s, err = p.Heatmap.ToPanel()
		exprs = p.Heatmap.Exprs
		done = true
	}
	if p.News != nil {
		s, err = p.News.ToPanel()
		done = true
	}
	if p.List != nil {
		s, err = p.List.ToPanel()
		done = true
	}
	if err != nil {
		return s, err
	}
	if !done {
		return s, fmt.Errorf("empty panel definition")
	}

	th := sdk.Thresholds{}
	// The panels are already made, now we syntax check the yaml and bail out if incorrect.
	// This makes the panel code shorter, as there is less error checking there.
	for _, expr := range exprs {
		if err := expr.ParsePromQL(); err != nil {
			return s, fmt.Errorf("%q is not valid promQL: %s", expr.Expr, err)
		}
		if expr.Legend == "" {
			return s, fmt.Errorf("a legend is mandatory for expr: %s", expr.Expr)
		}
		if expr.Threshold != "" {
			th1, err := parseThresholdPromQL(expr.Threshold)
			if err != nil {
				return nil, err
			}
			if th.Mode != "" && th.Mode != th1.Mode {
				return nil, fmt.Errorf("can not mixed absolute and percent thresholds: %s", expr.Threshold)
			}
			if th.Mode == "" {
				th.Mode = th1.Mode
			}
		}
	}
	return s, nil
}

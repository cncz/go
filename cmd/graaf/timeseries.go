package main

import (
	"fmt"

	"go.science.ru.nl/cmd/graaf/sdk"
)

// Timeseries is our representation of a TimeseriesPanel.
type Timeseries struct {
	Common `yaml:",inline"`
	Y      Y      `yaml:"y"`
	Stack  bool   `yaml:"stack"`
	Exprs  []Expr `yaml:"exprs"`
	Links  []Link `yaml:"links"`
}

// ToPanel converts a timeseries to TimeseriesPanel.
func (t *Timeseries) ToPanel() (*sdk.Panel, error) {
	if t.Title == "" {
		return nil, fmt.Errorf("panel '%s' should have a title", "timeseries")
	}
	if len(t.Exprs) == 0 {
		return nil, fmt.Errorf("panel '%s' should have an expr", "timeseries")
	}
	if t.Width == 0 {
		t.Width = 24
	}
	if t.Height == 0 {
		t.Height = 10
	}
	p := sdk.NewTimeseries(t.Title)
	th := sdk.Thresholds{}
	for i, expr := range t.Exprs {
		p.AddTarget(newTarget(i, expr))

		switch expr.Transform {
		case "negative-y":
			p.TimeseriesPanel.FieldConfig.Overrides = append(p.TimeseriesPanel.FieldConfig.Overrides, newOverride(i, expr))
		case "":
		default:
			return nil, fmt.Errorf("panel '%s' has transform with unknown parameter: %s", "timeseries", expr.Transform)
		}

		if expr.Threshold != "" {
			// error checked in dashboard.go#L200
			th1, _ := parseThresholdPromQL(expr.Threshold)
			th.Mode = th1.Mode
			th.Steps = append(th.Steps, th1.Steps...)
		}
	}
	if len(th.Steps) > 0 {
		th.Steps = append(fakeStep(), th.Steps...)
		p.TimeseriesPanel.FieldConfig.Defaults.Thresholds = th
	}

	t.Common.setPanel(p)
	// Y axes
	if err := t.Y.setFieldConfig(&p.TimeseriesPanel.FieldConfig); err != nil {
		return nil, err
	}
	// Data links.
	for _, l := range t.Links {
		if err := l.setFieldConfig(&p.TimeseriesPanel.FieldConfig); err != nil {
			return nil, err
		}
	}

	if t.Stack {
		p.TimeseriesPanel.FieldConfig.Defaults.Custom.Stacking.Group = "normal"
		p.TimeseriesPanel.FieldConfig.Defaults.Custom.Stacking.Mode = "normal"
		p.TimeseriesPanel.FieldConfig.Defaults.Custom.FillOpacity = 30
	}

	if t.Common.Color != "" {
		p.TimeseriesPanel.FieldConfig.Defaults.Color.Mode = "fixed"
		p.TimeseriesPanel.FieldConfig.Defaults.Color.SeriesBy = "last"
		p.TimeseriesPanel.FieldConfig.Defaults.Color.FixedColor = t.Common.Color
	}
	// increase pointsize if there are panel links to 8
	if len(t.Links) > 0 {
		p.TimeseriesPanel.FieldConfig.Defaults.Custom.PointSize = 8
	}

	return p, nil
}

func newOverride(i int, e Expr) sdk.Override {
	if i == len(letters) {
		i = len(letters) - 1
	}
	o := sdk.Override{
		Matcher: sdk.Matcher{
			ID:      "byFrameRefID",
			Options: string(letters[i]), // A, B, etc
		},
		Properties: []sdk.Property{
			{
				ID:    "custom.transform",
				Value: "negative-Y",
			},
		},
	}
	return o
}

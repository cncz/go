package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/prometheus/prometheus/promql/parser"
	"go.science.ru.nl/cmd/graaf/sdk"
)

// parseThresholdPromQL parses a threshold, in fake promql syntax.
// Like: 'critical{threshold="0%"}' or 'ok{threshold="0"}' for absolute.
func parseThresholdPromQL(e string) (sdk.Thresholds, error) {
	pq, err := parser.ParseExpr(e)
	if err != nil {
		return sdk.Thresholds{}, err
	}

	t := sdk.Thresholds{Mode: "absolute"}
	parser.Inspect(pq, func(node parser.Node, _ []parser.Node) error {
		vst, ok := node.(*parser.VectorSelector)
		if !ok {
			return nil
		}
		color := ""
		switch vst.Name {
		case "ok":
			color = "green"
		case "warning":
			color = "yellow"
		case "critical":
			color = "red"
		default:
			err = fmt.Errorf("expected 'ok,warning,critical', got %s", vst.Name)
			return err
		}
		t.Steps = []sdk.ThresholdStep{
			{Color: color},
		}
		return nil
	})
	// err set in parser.Inspect above
	if err != nil {
		return t, err
	}

	labels := parser.ExtractSelectors(pq)
	if len(labels) != 1 {
		return t, fmt.Errorf("expected only 1 label, got %d", len(labels))
	}
	for _, m := range labels[0] {
		if m.Name == "__name__" { // default label
			continue
		}
		if m.Name != "threshold" {
			return t, fmt.Errorf("Only a 'threshold' label is allowed, got %s", m.Name)
		}
		if strings.HasSuffix(m.Value, "%") {
			t.Mode = "percent"
			m.Value = m.Value[:len(m.Value)-1]
		}
		// needs to be a number
		v, err := strconv.ParseFloat(m.Value, 64)
		if err != nil {
			return t, fmt.Errorf("threshold label value needs to be a number, got %s", m.Value)
		}
		t.Steps[0].Value = &v
	}

	return t, nil
}

// for grafana do draw thresholds we need to have a 0th fake step to start the
// steps off with.
func fakeStep() []sdk.ThresholdStep {
	// no value, so it will be 'null'
	return []sdk.ThresholdStep{{Color: "green"}}

}

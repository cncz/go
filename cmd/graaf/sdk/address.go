package sdk

// Bool returns the address of the given bool value.
func Bool(b bool) *bool {
	return &b
}

// Int64 returns the address of the given int64 value.
func Int64(i int64) *int64 {
	return &i
}

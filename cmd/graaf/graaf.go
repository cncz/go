package main

import (
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"go.science.ru.nl/cmd/graaf/sdk"
	yaml "gopkg.in/yaml.v3"
)

func main() {
	flag.Parse()

	var buf []byte
	var err error

	dash := ""
	switch flag.NArg() {
	case 0:
		buf, err = io.ReadAll(os.Stdin)
		dash = "-"
	case 1:
		buf, err = os.ReadFile(flag.Arg(0))
		dash = flag.Arg(0)
	default:
		if flag.NArg() != 1 {
			log.Fatal("Expecting a single dashboard.yaml")
		}
	}
	if err != nil {
		log.Fatal(err)
	}
	b, err := unmarshal(buf)
	if err != nil {
		log.Fatal(err)
	}
	if err := b.IsValid(); err != nil {
		log.Fatalf("Invalid dashboard: %s", err)
	}
	data, err := generate(b)
	if err != nil {
		log.Fatalf("Failed to generate dashboard %q: %s", dash, err)
	}
	fmt.Println(string(data))
}

func unmarshal(buf []byte) (*Dashboard, error) {
	b := &Dashboard{}
	dec := yaml.NewDecoder(bytes.NewReader(buf))
	dec.KnownFields(true)
	err := dec.Decode(b)
	return b, err
}

func generate(d *Dashboard) (data []byte, err error) {
	b := sdk.NewBoard(d.Title)
	b.UID = newUID(d.UID)
	if d.Refresh != "" {
		_, err := time.ParseDuration(d.Refresh)
		if err != nil {
			return nil, fmt.Errorf("failed to parse refresh duration: %s", err)
		}
		b.Refresh = &sdk.BoolString{Flag: true, Value: d.Refresh}
	}
	if d.Editable != nil && *d.Editable == true {
		b.Editable = true
	}
	if d.Tooltip != "" {
		if d.Tooltip == "shared" {
			b.GraphTooltip = 1 // 0 = default, 2 = 'shared tooltip' (don't know what that is)
		} else {
			return nil, fmt.Errorf("tooltip can only have 'shared' as a value")
		}
	}

	b.AddTags(d.Tags...)
	if b.Templating, err = newTemplating(d.Vars); err != nil {
		return nil, err
	}
	if b.Links, err = newLinks(d.Links, true /* panel */); err != nil {
		return nil, err
	}
	for i, p := range d.Panels {
		panel, err := p.ToPanel()
		if err != nil {
			return nil, fmt.Errorf("panel %d: %s ", i, err)
		}
		if i > 0 {
			prev := b.Panels[i-1]
			// previous is width is 24, use it's y to increment ours by 10, but only if our y is not set and
			// we also are 24
			if *prev.CommonPanel.GridPos.W == 24 && *panel.CommonPanel.GridPos.W == 24 && *panel.CommonPanel.GridPos.Y == 0 {
				*panel.CommonPanel.GridPos.Y = *prev.CommonPanel.GridPos.Y + 10
			}
		}
		b.AddPanel(panel)
	}

	data, err = json.MarshalIndent(b, "", "  ")
	return data, err
}

func newUID(title string) string {
	h := sha1.New()
	h.Write([]byte(title))
	return hex.EncodeToString(h.Sum(nil))[:8]
}

func newTemplating(vars []Var) (sdk.Templating, error) {
	t := sdk.Templating{
		List: []sdk.TemplateVar{},
	}

	for _, v := range vars {
		if err := v.ParsePromQL(); err != nil {
			return t, fmt.Errorf("%q is not valid promQL: %s", v.Expr, err)
		}
	}

	for _, v := range vars {
		t.List = append(t.List, sdk.TemplateVar{
			Datasource: sdk.Datasource{Type: "prometheus", OrgID: 1, Name: "Prometheus"},
			Name:       v.Name,
			Regex:      v.Regex,
			Multi:      v.Multi,
			Sort:       varSort[v.Sort], // will be zero when not found
			Query: map[string]string{
				"query": v.Expr,
				"refId": "StandardVariableQuery",
			},
			Type:    "query",
			Refresh: sdk.BoolInt{Flag: false, Value: sdk.Int64(1)},
		})
	}
	return t, nil
}

var varSort = map[string]int{
	"none":         0,
	"alpha-asc":    1, // alpha-numerical ascending
	"alpha-desc":   2,
	"num-asc":      3, // numerical ascending
	"num-desc":     4,
	"alpha-asc-i":  5, // case insensitive
	"alpha-desc-i": 6,
}

func newLinks(links []Link, panel bool) ([]sdk.Link, error) {
	l := make([]sdk.Link, len(links))
	for i, link := range links {
		if link.Title == "" {
			return nil, fmt.Errorf("links must have a title")
		}
		l[i] = sdk.Link{
			AsDropdown: sdk.Bool(true),
			Title:      link.Title,
		}
		if panel && !link.External {
			l[i].KeepTime = sdk.Bool(true)
			l[i].IncludeVars = true
		}
		switch {
		case link.URL != "":
			l[i].Type = "link"
			l[i].URL = &link.URL

		case len(link.Tags) > 0:
			l[i].Type = "dashboards"
			l[i].KeepTime = sdk.Bool(true)
			l[i].IncludeVars = true

		}
	}
	return l, nil
}

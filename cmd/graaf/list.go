package main

import (
	"fmt"

	"go.science.ru.nl/cmd/graaf/sdk"
)

// List is our representation of a DashlistPanel.
type List struct {
	Common `yaml:",inline"`
}

// ToPanel converts a list to DashlistPanel.
func (n *List) ToPanel() (*sdk.Panel, error) {
	if n.Title == "" {
		return nil, fmt.Errorf("panel '%s' should have a title", "list")
	}
	p := sdk.NewDashlist(n.Title)
	p.DashlistPanel.Recent = true
	p.DashlistPanel.Starred = true
	n.Common.setPanel(p)
	return p, nil
}

package main

import (
	"fmt"

	"go.science.ru.nl/cmd/graaf/sdk"
)

// Heapmap is our representation of a Heatmap pabel.
type Heatmap struct {
	Common `yaml:",inline"`
	Y      Y      `yaml:"y"`
	Exprs  []Expr `yaml:"exprs"`
	Links  []Link `yaml:"links"`
}

// ToPanel converts a timeseries to TimeseriesPanel.
// Mostly TODO(miek)
func (t *Heatmap) ToPanel() (*sdk.Panel, error) {
	if t.Title == "" {
		return nil, fmt.Errorf("panel '%s' should have a title", "timeseries")
	}
	if len(t.Exprs) == 0 {
		return nil, fmt.Errorf("panel '%s' should have an expr", "timeseries")
	}
	if t.Width == 0 {
		t.Width = 24
	}
	if t.Height == 0 {
		t.Height = 10
	}
	p := sdk.NewHeatmap(t.Title)
	for i, expr := range t.Exprs {
		p.AddTarget(newTarget(i, expr))

		// thesholds?
		/*
			switch expr.Transform {
			case "negative-y":
				p.TimeseriesPanel.FieldConfig.Overrides = append(p.TimeseriesPanel.FieldConfig.Overrides, newOverride(i, expr))
			case "":
			default:
				return nil, fmt.Errorf("panel '%s' has transform with unknown parameter: %s", "timeseries", expr.Transform)
			}
		*/
	}

	t.Common.setPanel(p)
	return p, nil
	/*
		// Y axes
		if err := t.Y.setFieldConfig(&p.HeatmapPanel.FieldConfig); err != nil {
			return nil, err
		}
		// Data links.
		for _, l := range t.Links {
			if err := l.setFieldConfig(&p.HeatmapPanel.FieldConfig); err != nil {
				return nil, err
			}
		}

		if t.Common.Color != "" {
			p.HeatmapPanel.FieldConfig.Defaults.Color.Mode = "fixed"
			p.HeatmapPanel.FieldConfig.Defaults.Color.SeriesBy = "last"
			p.HeatmapPanel.FieldConfig.Defaults.Color.FixedColor = t.Common.Color
		}
		// increase pointsize if there are panel links, 8
		if len(t.Links) > 0 {
			p.HeatmapPanel.FieldConfig.Defaults.Custom.PointSize = 8
		}

		return p, nil
	*/
}

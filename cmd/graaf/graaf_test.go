package main

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestGraaf(t *testing.T) {
	dir := "testdata"
	testFiles, err := ioutil.ReadDir(dir)
	if err != nil {
		t.Fatalf("could not read %s: %q", dir, err)
	}
	for _, f := range testFiles {
		if f.IsDir() {
			continue
		}

		if filepath.Ext(f.Name()) != ".yaml" {
			continue
		}
		buf, err := os.ReadFile(filepath.Join("testdata", f.Name()))
		if err != nil {
			t.Fatal(err)
		}
		b, err := unmarshal(buf)
		if err != nil {
			t.Fatal(err)
		}
		if err := b.IsValid(); err != nil {
			t.Fatalf("Invalid dashboard: %s", err)
		}
		data, err := generate(b)
		if err != nil {
			log.Fatalf("Failed to generate dashboard: %s", err)
		}
		expect := f.Name()[:len(f.Name())-5]
		json, err := os.ReadFile(filepath.Join("testdata", expect+".json"))
		if err != nil {
			t.Fatal(err)
		}
		jsonstr := strings.TrimSpace(string(json))
		datastr := strings.TrimSpace(string(data))
		if jsonstr != datastr {
			t.Fatalf("Generated dashboard does not match testdata: %s", expect+".json")
		}
	}
}

func TestExample(t *testing.T) {
	buf, err := os.ReadFile("example.yaml")
	if err != nil {
		t.Fatal(err)
	}
	b, err := unmarshal(buf)
	if err != nil {
		t.Fatal(err)
	}
	if err := b.IsValid(); err != nil {
		t.Fatalf("Invalid dashboard: %s", err)
	}
	if _, err := generate(b); err != nil {
		log.Fatalf("Failed to generate dashboard: %s", err)
	}
}

package main

import (
	"fmt"

	"go.science.ru.nl/cmd/graaf/sdk"
)

// News is our representation of a NewsPanel.
type News struct {
	Common `yaml:",inline"`
	Links  []Link `yaml:"links"`
}

// ToPanel converts a news to NewsPanel.
func (n *News) ToPanel() (*sdk.Panel, error) {
	if n.Title == "" {
		return nil, fmt.Errorf("panel '%s' should have a title", "news")
	}
	if len(n.Links) != 1 {
		return nil, fmt.Errorf("a news panel must have a single link")
	}
	p := sdk.NewNews(n.Title)
	p.NewsPanel.Options.FeedUrl = n.Links[0].URL
	n.Common.setPanel(p)
	return p, nil
}

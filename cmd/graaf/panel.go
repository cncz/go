package main

import (
	"fmt"

	"go.science.ru.nl/cmd/graaf/sdk"
)

// Common specifies some common attributes for all YAML panels.
type Common struct {
	Title       string `yaml:"title"`
	Width       int    `yaml:"width"`
	Height      int    `yaml:"height"`
	Color       string `yaml:"color"`
	Grid        Grid   `yaml:"grid"`
	Transparent bool   `yaml:"transparent"`
}

// Grid is the exact positioning with X and Y coordinates.
type Grid struct {
	X int `yaml:"x"`
	Y int `yaml:"y"`
}

// Y is the y-axes.
type Y struct {
	Unit     string   `yaml:"unit"`
	Decimals *int     `yaml:"decimals"`
	Min      *float64 `yaml:"min"`
	Max      *float64 `yaml:"max"`
	Scale    string   `yaml:"scale"`
	Center   string   `yaml:"center"`
}

const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

// setPanel applies graaf's Common to the sdk's CommonPanel.
func (c Common) setPanel(p *sdk.Panel) {
	// ignore Title as that is set via the sdk/New* functions.
	p.CommonPanel.GridPos.W = &c.Width
	p.CommonPanel.GridPos.H = &c.Height
	p.CommonPanel.GridPos.X = &c.Grid.X
	p.CommonPanel.GridPos.Y = &c.Grid.Y
	p.CommonPanel.Transparent = c.Transparent
}

// setFieldConfig set the field config in f using the values from y.
func (y Y) setFieldConfig(f *sdk.FieldConfig) error {
	if y.Unit != "" {
		f.Defaults.Unit = y.Unit
	}
	if y.Decimals != nil {
		f.Defaults.Decimals = y.Decimals
	}
	switch y.Unit {
	case "percent":
		min := float64(0)
		max := float64(100)
		f.Defaults.Min = &min
		f.Defaults.Max = &max
	case "percentunit":
		min := float64(0)
		max := float64(1.0)
		f.Defaults.Min = &min
		f.Defaults.Max = &max
	}

	if y.Min != nil {
		f.Defaults.Min = y.Min
	}
	if y.Max != nil {
		f.Defaults.Max = y.Max
	}
	switch y.Scale {
	case "log2":
		f.Defaults.Custom.ScaleDistribution.Type = "log"
		f.Defaults.Custom.ScaleDistribution.Log = 2
	case "log10":
		f.Defaults.Custom.ScaleDistribution.Type = "log"
		f.Defaults.Custom.ScaleDistribution.Log = 10
	}

	// Set threshold line mode to dashed - this can not be overriden.
	f.Defaults.Custom = &sdk.FieldConfigCustom{}
	f.Defaults.Custom.ThresholdsStyle.Mode = "dashed"
	f.Defaults.Custom.DrawStyle = "line"
	f.Defaults.Custom.LineWidth = 1
	f.Defaults.Custom.PointSize = 2
	f.Defaults.Custom.LineStyle.Fill = "solid"
	f.Defaults.Custom.ShowPoints = "auto"

	switch y.Center {
	case "zero":
		f.Defaults.Custom.AxisCenteredZero = sdk.Bool(true)
	case "":
	default:
		return fmt.Errorf("Only 'zero' is permitted for 'center'")
	}

	return nil
}

func (l Link) setFieldConfig(f *sdk.FieldConfig) error {
	l1, err := newLinks([]Link{l}, false /* not a panel */)
	if err != nil {
		return err
	}
	f.Defaults.Links = append(f.Defaults.Links, l1[0])
	return nil
}

// newTarget creates a ready to use pointer to a sdk.Target with the datasource set to Prometheus.
func newTarget(i int, e Expr) *sdk.Target {

	t := &sdk.Target{Expr: e.Expr, LegendFormat: e.Legend}

	if i == len(letters) {
		i = len(letters) - 1
	}
	t.RefID = string(letters[i])
	t.Datasource = sdk.Datasource{Type: "prometheus", OrgID: 1, IsDefault: true}
	return t
}

package main

import (
	"fmt"

	"go.science.ru.nl/cmd/graaf/sdk"
)

// Stat is our representation of a StatPanel.
type Stat struct {
	Common `yaml:",inline"`
	Y      Y      `yaml:"y"`
	Exprs  []Expr `yaml:"exprs"`
	Links  []Link `yaml:"links"`
}

// ToPanel converts a stat to a StatPanel.
func (s *Stat) ToPanel() (*sdk.Panel, error) {
	if s.Title == "" {
		return nil, fmt.Errorf("panel '%s' should have a title", "stat")
	}
	if len(s.Exprs) == 0 {
		return nil, fmt.Errorf("panel '%s' should have an expr", "stat")
	}
	if len(s.Exprs) > 1 {
		return nil, fmt.Errorf("panel '%s' can only have a single expr", "stat")
	}
	if s.Width == 0 {
		s.Width = 5
	}
	if s.Height == 0 {
		s.Height = 5
	}
	p := sdk.NewStat(s.Title)
	th := sdk.Thresholds{}
	for i, expr := range s.Exprs {
		p.AddTarget(newTarget(i, expr))

		if expr.Threshold != "" {
			// error checked in dashboard.go#L200
			th1, _ := parseThresholdPromQL(expr.Threshold)
			th.Mode = th1.Mode
			th.Steps = append(th.Steps, th1.Steps...)
		}
	}
	if len(th.Steps) > 0 {
		th.Steps = append(fakeStep(), th.Steps...)
		p.TimeseriesPanel.FieldConfig.Defaults.Thresholds = th
	}

	s.Common.setPanel(p)
	if s.Y.Scale != "" {
		return nil, fmt.Errorf("panel '%s' can not have scale set", "stat")
	}
	// Y axes.
	s.Y.setFieldConfig(&p.StatPanel.FieldConfig)
	// Data links.
	for _, l := range s.Links {
		if err := l.setFieldConfig(&p.StatPanel.FieldConfig); err != nil {
			return nil, err
		}
	}
	// t.Common.Color handling.

	return p, nil
}

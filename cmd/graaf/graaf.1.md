%%%
title = "graaf 1"
area = "User Commands"
workgroup = "C&CZ"
%%%

## Name

graaf - generate Grafana JSON from smaller YAML dashboards

## Synopsis

graaf *`[FILE]`*

## Description

Create grafana dashboards from simple(r) YAML files. Graaf acts like a filter and ingests YAML and
spits out Grafana (9+) compatible JSON. Graaf can be used to provision Grafana from git and to make
humans _not_ review long JSON dashboards, but much shorter YAML ones. The promQL used in the graphs
is syntax checked during the generation.

If no **FILE** is given the input is read from standard input. The created JSON is printed to
standard output. There are no options.

Graaf also makes your dashboards consistent and takes the fun out of toying with Grafana for hours
to make your thing look just right.

Graaf means 'to dig' and 'count' (the noble rank) in Dutch.

### Semantic Checks

Each promQL expression *must* have a *job* matcher to make sure you're dealing with the right
metric. An expression with only a `topk` or `bottomk` is also disallowed, as this retrieves way to
many metrics. This can be overridden *in the YAML*, with the `allow: [topk, bottomk]` under an
expression that uses it.

As single (floating point) number in an expression is allowed.

### Dashboards and Panels

Supported are timeseries, heatmaps and stat panels. For grafana homepage tweaking, dashlist and the
news panel is also supported.

A sample dashboard would look something like this:

~~~ yaml
title: My example dashboard
uid: stable identifier for this dashboard
panels:
  - timeseries:
     title: Active Memory
     width: 24
     y:
      unit: "decbytes"
      min: 0
     exprs:
       - expr: 'node_memory_Active_bytes{job="node"}'
         legend: "{{__name__}}"
~~~

With `graaf dashboard.yaml | grep '"uid":'` you can check what the UID of a dashboard is, this is
important when linking to other dashboards, i.e. the link then becomes `.../d/<uid>`.

The point size in timeseries is set to 2 (really small) and to 8 (rather large) if the are panel
links.

### Columns and Rows

Grafana uses a simple grid, where the total width is 24, and each row is 10 high. So having a `grid`
with `y: 10` means the second row.

If you omit `y` and `width`, the width will default to 24. The y value will be unset unless the
previous dashboard and this one have a width of 24, then the y value will the previous one plus 10.
In other words, if you have a dashboard full of timeserie panels they all will be full widith and be
correctly placed.

### Links

Dashboard links will have the current time and current instance variables attached to them
automatically. If you don't need this, use `external: true`.

For data links see
<https://grafana.com/docs/grafana/v9.3/panels-visualizations/configure-data-links/#data-links>.
If a panel has data links the dot size of the pointer is increased to signify there is more to it
(as Grafana has no good way of signaling this otherwise...)

Panel links are not supported as these are barely discoverable in the UI.

### Dashboards

Each dashboard has an uid that gets generated from the title by SHA1-summing it and taking the first
8 characters. This helps generate fixed dashboard URLs, so data links keep on working. Other options
include:

* `title`: title for the dashboard.
* `uid`: stable identifier for this dashboard. This must be unique among dashboards!
* `vars`: holds all variable definitions in `var`:
    - `name`: for the variable
    - `expr`: promQL query, must have a `job=` matcher.
    - `regex`: regex to parse expr: e.g. `/.*job="(?<value>.*)".*/`, the other supported capture
      group is `(?<text>)`.
    - `multi`: allow selecting multiple values
    - `sort`: enable sorting allowed values:
        - alpha-asc  (alphanumerical ascending)
        - alpha-desc
        - num-asc (numerical ascending)
        - num-desc
        - alpha-asc-i (alphanumerical ascending and case insensitive)
        - alpha-desc-i
* `links`: dashboard links, see below they are identical in their setup.
* `refresh`: if you want a dashboard to refresh (in kiosk mode for instance), add this, needs a Go
  time.Duration string.
* `editable`: default false, set to true if you want everyone to be able to edit (but not save) a
  dashboard.

#### Shared Parameters for all Panels

A dashboard contains multiple panels. Each panels has the following properties.

* `width`: 24 units is the entire screen, stat panel uses 5 by default.
* `height`: height of the panel, 10 is good (and the default), stat panels are 5.
* `grid`: that has x, and y values for exact placement in
  [grid](https://grafana.com/docs/grafana/v9.0/dashboards/json-model/#panels). So placing two panels
  on the same level would entail:
  ~~~ yaml
  grid:
      x: 0
      y: 0
  ~~~
  and the next panel:
  ~~~ yaml
  grid:
      x: 12
      y: 0
  ~~~
  At same time assuming the `width: 12` is set on both to make 24 in total. Panels have default
  height of 10, to incrementing y with 10 is a new row.

* `color`: specify the color for the graph, a single color will apply to all exprs.
* `y`: the Y axes definition. If not specified it will be "hidden". Has:
    - `unit` specifies the unit for the Y axes, needs to be a grafana one which can be seen with
      inspect panel JSON or see [the definitions in grafana](https://github.com/grafana/grafana/blob/main/packages/grafana-data/src/valueFormats/categories.ts).
    - `decimals` number of decimals places after the dot.
    - `min` set minimal value
    - `max` set maximum value
    - `center: zero`, center the zero value
* `tags`: a list of tags for this dashboard. Helps with searching.
* `links`: a list of *Data* `Link`s, note Panel Links are not supported.
    - `title`: title for the link
    - `url`: set when pointing to external URL
    - `tags`: set when pointing to dashboards with these tags
    - `external`: set to true if the link is an external one.
  See <https://grafana.com/docs/grafana/v9.0/panels/configure-data-links/> for what can be used
  inside these links. Especially variable usage as `${__field.labels.instance}` and `${__value.time}`
  if set. An example would then be:

    ~~~ yaml
    links:
     - title: "CPU details for ${__field.labels.instance}"
         url: "/d/9fc1f0c0/cpu?var-instance=${__field.labels.instance}"
    ~~~

  Where `var-<name>` is the way to give the new panel a variable. For dashboard links the time and
  variables are kept. If it's a external link you can suppress this, by setting 'external: true'.
* `tooltip`: can only have one option: `shared`, which enables the shared crosshair on the
  dashboard, omit to get the default.

* `transparent`: a boolean indicating the panel should be transparent, default is `false`.
* `exprs`: hold the prometheus expressions.
    - `expr`: the promQL
    - `legend`: specify what should be in the legend
    - `transform`: how to transform the y axes
      - `negative-y` is the current only supported value.
    - `threshold`: draw a (dashed) threshold line. These are _also_ specified in a promQL syntax:
        * `ok|warning|critical{threshold="VALUE[%]"}`, where `threshold=VALUE` is where the threshold
         is to be drawn. If a %-sign is used, you want it to be a percentage instead of absolute.
         `ok`, `warning` or `critical` defined the color of the lines, green, yellow or red
         respectively.

See <https://grafana.com/blog/2019/12/10/pro-tips-dashboard-navigation-using-links/> for all types
of links panels can have.

#### Timeseries Panel

See <https://grafana.com/docs/grafana/v9.0/visualizations/time-series/>

Use `timeseries` in the YAML.

Extra supported are:

* `Stack: true`, stack the metrics.
* For `y` you can:
    - `scale` set scale: valid options: log2, log10, not specifying results in linear.
    With some units the following is done
    * `percent`: set min to 0 and max to 100 (if Min and Max isn't set).
    * `percentunit`: set min to 0.0 and max to 1.0 (if Min and Max isn't set).

#### Stat Panel

See <https://grafana.com/docs/grafana/v9.0/visualizations/stat-panel/>

Use `stat` in the YAML to have a "stat" panel, here is an example:

~~~ yaml
- stat:
    title: Active Memory Usage for $hello
    grid:
      x: 12
      y: 0
    y:
      unit: "decbytes"
      decimals: 1
    exprs:
      - expr: 'node_memory_Active_bytes{job="node"}'
        legend: "Bytes"
~~~

#### Heatmap Panel

See <<https://grafana.com/docs/grafana/v9.0/visualizations/heatmap/>


#### News Panel

This is used on the home panel in Grafana, you can link it to an RSS feed and show that in grafana:

~~~ yaml
- news:
    title: Latest news
    links:
      - url: 'https://localhost/news.xml'
~~~

#### List Panel

Use `list` in the YAML to have a Dashlist Panel that lists recent and starred panel. This is useful
in the home dashboard.

### Developer Notes

In `sdk/*` we have the definitions as used by grafana in the top level directory of graaf we have
struct definitions used to parse our YAML files.

While working on graaf you probably want to load the generated dashboard into grafana. The easiest
way to do this is:

- login into Grafana as admin (if you disabled the login icon, just go to /login).
- generated your test dashboard (set `editable: true` to be able to edit it).
- in Grafana go to upper right, click the plus and choose "Import Dashboard".
- See if it works, and use "Inspect"->"Panel JSON" to see what Grafana did with your JSON.

## Grafana Setup

For Grafana to work you only may use OrgID 1 and provision the datasource and dashboards via the
following YAML config:

In `.../provisioning/dashboards/dashboards.yaml`, put:

~~~ yaml
apiVersion: 1

providers:
 - name: 'default'
   orgId: 1
   folder: ''
   folderUid: ''
   type: file
   options:
     path: /var/lib/grafana/dashboards
~~~

And save the output of `graaf` in /var/lig/grafana/dashboards. Next configure a datasource in
`../provisioning/datasources/prometheus.yaml`:

~~~ yaml
apiVersion: 1

datasources:
- name: Prometheus
  type: prometheus
  access: proxy
  orgId: 1
  url: http://localhost:9090
  isDefault: true
~~~

For good measure erase the grafana database: `rm /var/lib/grafana/grafana.db` and restart grafana.
If you want to set a default 'home' dashboard use:

~~~ ini
[dashboards]
default_home_dashboard_path = /var/lib/grafana/dashboards/mydashboard.json
~~~

in `/etc/grafana/grafana.ini`.

## Bugs

Heatmap panel is barely finished.

## Author

Miek Gieben <miek@miek.nl>

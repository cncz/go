package main

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
	"strings"
)

type HostValue struct {
	Type  string // what was extracted
	Host  string
	Value float64
}

// If a hostname has -rc.net in it, that will be removed.
// This assumes:
// Columns: host_name description perf_data\n
// hostname and perfdata, perfdat is temp=22;43;47;;  temp=xx (is what we parse)
// A more complete example:
// "smtp3.science.ru.nl","Mailstats","delta_t=37;;;; intern_in=1;;;; intern_delivered=2;;;; intern_rejected=0;;;; extern_in=0;;;; extern_out=0;;;; extern_rejected=0;;;;]
//
// so split on space, then check the semi-colons, each one of these becomes a label in the metric
func Parse(r io.Reader) ([]HostValue, error) {
	data, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}
	var result [][]string
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	if len(result) < 1 {
		return nil, fmt.Errorf("can not parse, not enough fields")
	}
	hvs := []HostValue{}
	for i := range result {
		list := result[i]
		if len(list) != 3 {
			return nil, fmt.Errorf("can not parse, not enough fields")
		}

		hostname := list[0]
		hostname = strings.Replace(hostname, "-rc.net", "", 1) + ":9100"

		switch list[2] {
		case "":
			hvs = append(hvs, HostValue{Host: hostname})
		default:
			perfdata := strings.Fields(list[2])
			for _, pd := range perfdata {
				if pd[0] == '[' { // signals the script that gave us these values, skip
					continue
				}
				hv := HostValue{Host: hostname}

				sc := strings.Index(pd, ";")
				if sc == -1 {
					return nil, fmt.Errorf("can not parse, no semicolon found in field: %q", pd)
				}

				value := pd[:sc]
				eq := strings.Index(value, "=")
				if eq == -1 {
					return nil, fmt.Errorf("can not parse, no equal found in field: %q", value)
				}
				hv.Type = value[:eq]
				value = value[eq+1:]
				val, err := strconv.ParseFloat(value, 64)
				if err != nil {
					return nil, err
				}
				hv.Value = val
				hvs = append(hvs, hv)
			}
		}
	}

	return hvs, nil
}

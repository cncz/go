package main

import (
	"fmt"
	"net"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"go.science.ru.nl/log"
)

type Metric struct {
	Name   string // generic name, not used, by handy to spot metrics the (long) list in main.go
	Msg    string // LQL query.
	Metric string // metric name, checkmk_ will be prepended. Only gauges are supported.
	Type   bool   // when true, the metric has a type= label because the returned data has multiple data points

	g *prometheus.GaugeVec
}

// MakeMetrics will create all the metrics and fills out mc and mg members of *Metric.
func MakeMetrics(ms []*Metric) error {
	for i := range ms {
		log.Infof("Making metric: '%s_%s'", "checkmk", ms[i].Metric)
		if ms[i].Type {
			ms[i].g = promauto.NewGaugeVec(prometheus.GaugeOpts{Namespace: "checkmk", Name: ms[i].Metric}, []string{"instance", "type"})
			continue
		}
		ms[i].g = promauto.NewGaugeVec(prometheus.GaugeOpts{Namespace: "checkmk", Name: ms[i].Metric}, []string{"instance"})
	}
	return nil
}

// Write writes the LQL query to the socket c.
func (m *Metric) Write(c net.Conn) error {
	msg := "GET services\n" + m.Msg + "\nOutputFormat: json\n"
	log.Infof("Writing %q for metric %q", msg, m.Name)
	_, err := fmt.Fprint(c, msg)
	if err != nil {
		return err
	}
	return c.(*net.UnixConn).CloseWrite()
}

// Export exports the metric m with the values from hv.
func (m *Metric) Export(hv HostValue) {
	if m.Type {
		m.g.WithLabelValues(hv.Host, hv.Type).Set(hv.Value)
		return
	}
	m.g.WithLabelValues(hv.Host).Set(hv.Value)
}

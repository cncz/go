package main

import (
	"fmt"
	"os"
	"testing"
)

func TestParsePower(t *testing.T) {
	f, _ := os.Open("testdata/power.json")
	hvs, err := Parse(f)
	if err != nil {
		t.Fatal(err)
	}
	got := fmt.Sprintf("%v", hvs)
	t.Logf("%s", got)
	expected := `[{ vos.science.ru.nl:9100 0} {power wayback.science.ru.nl:9100 192} {power zwik2.science.ru.nl:9100 144}]`
	if got != expected {
		t.Fatal("didn't get expected result")
	}
}

func TestParseQueue(t *testing.T) {
	f, _ := os.Open("testdata/queue.json")
	hvs, err := Parse(f)
	if err != nil {
		t.Fatal(err)
	}
	got := fmt.Sprintf("%v", hvs)
	expected := `[{mailq mx1.science.ru.nl:9100 11} {mailq mx2.science.ru.nl:9100 9} {mailq zaaivm.science.ru.nl:9100 8}]`
	t.Logf("%s", got)
	if got != expected {
		t.Fatal("didn't get expected result")
	}
}

func TestParseMail(t *testing.T) {
	f, _ := os.Open("testdata/mail.json")
	hvs, err := Parse(f)
	if err != nil {
		t.Fatal(err)
	}
	got := fmt.Sprintf("%v", hvs)
	expected := `[{delta_t mx1.science.ru.nl:9100 16} {intern_in mx1.science.ru.nl:9100 0} {intern_delivered mx1.science.ru.nl:9100 1} {intern_rejected mx1.science.ru.nl:9100 0} {extern_in mx1.science.ru.nl:9100 1} {extern_out mx1.science.ru.nl:9100 0} {extern_rejected mx1.science.ru.nl:9100 0} {delta_t zaaivm.science.ru.nl:9100 0} {intern_in zaaivm.science.ru.nl:9100 0} {intern_delivered zaaivm.science.ru.nl:9100 0} {intern_rejected zaaivm.science.ru.nl:9100 0} {extern_in zaaivm.science.ru.nl:9100 0} {extern_out zaaivm.science.ru.nl:9100 0} {extern_rejected zaaivm.science.ru.nl:9100 0}]`
	t.Logf("%s", got)
	if got != expected {
		t.Fatal("didn't get expected result")
	}
}

func TestParseUsers(t *testing.T) {
	f, _ := os.Open("testdata/user.json")
	hvs, err := Parse(f)
	if err != nil {
		t.Fatal(err)
	}
	got := fmt.Sprintf("%v", hvs)
	expected := `[{users wayback.science.ru.nl:9100 0} {users weber.science.ru.nl:9100 0} {users webvm01.science.ru.nl:9100 4}]`
	t.Logf("%s", got)
	if got != expected {
		t.Fatal("didn't get expected result")
	}
}

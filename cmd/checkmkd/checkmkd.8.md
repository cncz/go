%%%
title = "checkmkd 8"
area = "Monitoring Commands"
workgroup = "Prometheus"
%%%

checkmkd
=====

## Name

checkmkd - query checkmk and export prometheus metrics

## Synopsis

checkmkd  *[OPTION]*...

## Description

checkmkd will loop and query Checkmk via LQL and export Prometheus metrics for the data found. The
metricsa (only gauges are supported) exported are defined in main.go (there is no configuration file).

The machine names are put in the "instance" label, so you need `honor_labels` in prometheus to not
overwrite that label with the hostname running checkmkd. To create parity with node-exporter the
instance labels are suffixed with ":9100".

Options are:

`-s`=*PATH*
:   *PATH* of checkmk unix socket

`-p`=*PORT*
:   *PORT* number for prometheus webserver

`-v`
:   show version

## See Also

Prometheus and Checkmk.

## Author

Miek Gieben <miek@miek.nl>.

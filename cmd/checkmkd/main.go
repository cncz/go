package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.science.ru.nl/log"
)

var version = "n/a"

var (
	flagSocket  = flag.String("s", fmt.Sprintf("%s/tmp/run/live", os.Getenv("OMD_ROOT")), "path to unix socket")
	flagPort    = flag.Int("p", 9101, "prometheus port")
	flagVersion = flag.Bool("v", false, "show version")
)

var Metrics = []*Metric{
	// Temp, Power
	{
		Name:   "Temperature Inlet",
		Msg:    "Columns: host_name description perf_data\nFilter: description ~ Temperature System Board Inlet",
		Metric: "inlet_temp_celcius",
	},
	{
		Name:   "Temperature Exhaust",
		Msg:    "Columns: host_name description perf_data\nFilter: description ~ Temperature System Board Exhaust",
		Metric: "exhaust_temp_celcius",
	},
	{
		Name:   "Power Consumption",
		Msg:    "Columns: host_name description perf_data\nFilter: description ~ System Board Pwr Consumption",
		Metric: "system_power_consumption_watt",
	},
	// Mail
	{
		Name:   "Mail Queue",
		Msg:    "Columns: host_name description perf_data\nFilter: description ~ Mailqueue",
		Metric: "service_mail_queue_length",
	},
	{
		Name:   "Mail Stats",
		Msg:    "Columns: host_name description perf_data\nFilter: description ~ Mailstats",
		Metric: "service_mail_stats",
		Type:   true,
	},
	// Users
	{
		Name:   "User Count",
		Msg:    "Columns: host_name description perf_data\nFilter: description ~ Users",
		Metric: "system_users_count",
		Type:   true,
	},
}

func main() {
	flag.Parse()

	if *flagVersion {
		fmt.Println(version)
		os.Exit(0)
	}

	MakeMetrics(Metrics)

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	ticker := time.NewTicker(time.Second * 21)
	timer := time.NewTimer(1 * time.Second)

	http.Handle("/metrics", promhttp.Handler())
	log.Infof("Starting Prometheus on port %d", *flagPort)
	go func() {
		err := http.ListenAndServe(fmt.Sprintf(":%d", *flagPort), nil)
		if err != nil {
			log.Fatalf("Could not start server: %s", err)
		}
	}()

	log.Info("Querying checkmk")
	for {
		select {
		case <-done:
			os.Exit(0)
		case <-ticker.C:
		case <-timer.C: // quickly start the first time
		}
		for _, m := range Metrics {
			// be able to interrupt a long list of metrics
			select {
			case <-done:
				os.Exit(0)
			default:
			}

			c, err := net.Dial("unix", *flagSocket)
			if err != nil {
				log.Warningf("Failed to dial unix socket %s: %s", *flagSocket, err)
				time.Sleep(1 * time.Second)
				continue
			}

			err = m.Write(c)
			if err != nil {
				log.Warningf("Failed to write to unix socket %s: %s", *flagSocket, err)
				time.Sleep(1 * time.Second)
				continue
			}

			hvs, err := Parse(c)
			if err != nil {
				log.Warningf("Failed to parse reply: %s", err)
				continue
			}
			c.Close()

			log.Infof("Exporting metric: %q with %d host values", m.Name, len(hvs))
			for i := range hvs {
				m.Export(hvs[i])
			}
		}
	}
}

package main

import (
	"flag"
	"fmt"
	"os/exec"
	"os/user"
	"strconv"
	"syscall"

	"go.science.ru.nl/log"
)

var (
	flagArg0  = flag.String("arg0", "", "how to rename argv0")
	flagUid   = flag.String("uid", "", "uid to use (user name will be resolved)")
	flagGid   = flag.String("gid", "", "gid to use (group name will be resovled)")
	flagDebug = flag.Bool("debug", false, "wait for the command and show its ouput")
)

func main() {
	flag.Parse()

	if len(flag.Args()) == 0 {
		log.Fatal("need a command")
	}

	var err error
	uid := int64(0)
	gid := int64(0)

	cur, _ := user.Current()
	if *flagUid == "" {
		*flagUid = cur.Uid
	}
	if *flagGid == "" {
		*flagGid = cur.Gid
	}

	uid, err = strconv.ParseInt(*flagUid, 10, 32)
	if err != nil { // it was a user name
		u, err := user.Lookup(*flagUid)
		if err != nil {
			log.Fatalf("failed to look up user %s: %s", *flagUid, err)
		}
		uid, _ = strconv.ParseInt(u.Uid, 10, 32)
	}

	gid, err = strconv.ParseInt(*flagGid, 10, 32)
	if err != nil { // it was a group name
		g, err := user.LookupGroup(*flagGid)
		if err != nil {
			log.Fatalf("failed to look up group %s: %s", *flagGid, err)
		}
		gid, _ = strconv.ParseInt(g.Gid, 10, 32)
	}

	path := flag.Args()[0]
	args := flag.Args()[1:]

	cmd := exec.Command(path, args...)
	cmd.Args[0] = *flagArg0

	log.Infof("Starting process for %s with %v", path, cmd.Args)

	// Setpgid is needed if we need a controlling terminal
	cmd.SysProcAttr = &syscall.SysProcAttr{Foreground: false}
	if cur.Uid == "0" { // only root can set this
		cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(uid), Gid: uint32(gid)}
	}

	if *flagDebug {
		out, err := cmd.CombinedOutput()
		fmt.Printf("%s", out)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}
	log.Infof("Process started as %d", cmd.Process.Pid)
	// no cmd.Wait(), let is be reaped by PID 1.
}

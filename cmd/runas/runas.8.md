%%%
title = "runas 8"
area = "Runas Manual"
workgroup = "C&CZ"
%%%

## Name

runas - set process attributes before exec-ing command

## Synopsis

runas [**OPTIONS**] *command* [**ARGS**]...

## Description

Runas runs *command* with optionally a different argv0 and with extra or under a different uid or
gid. It does not wait for its completion and the process is run in the background. It is expected
that init (PID 1) will reap the process once its exits.

## Options

`-arg0` **ARG0**
:  set **ARG0** as the command's argv0.

`-uid` **UID**
:  run under this **UID**, if this is a user name it will be resolved to a uid.

`-gid` **GID**
:  run under this primary **GID**, if this is a group name it will be resolved to a gid.

`-debug`
: wait for the process to finish a print its output.

## Environment

The process' environment is cleared before starting.

## Author

Miek Gieben <miek@miek.nl>

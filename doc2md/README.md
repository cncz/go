# doc2md

This is forked from <a
href="https://github.com/davecheney/godoc2md">https://github.com/davecheney/godoc2md</a>.  The
primary difference being that this version is a library that can be used by other packages.

This a fork of a fork <https://github.com/WillAbides/godoc2md>. But cleanup and simplified.
Point it to a Go repo on disk and generate markdown from it.

Further modified to just work for science.ru.nl URLs.

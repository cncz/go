package doc2md

import (
	"testing"
)

func TestURLForFile(t *testing.T) {
	u := urlForFile("go.science.ru.nl/dns/scan.go", "/miekg/dns", "main", "/")
	if exp := "https://gitlab.science.ru.nl/cncz/go/-/blob/main/miekg/dns/scan.go"; u != exp {
		t.Errorf("expected %s, got %s", exp, u)
	}
}

// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package doc2md creates a markdown representation of a package's godoc.
//
// This is forked from https://github.com/davecheney/godoc2md.  The primary difference being that this version is
// a library that can be used by other packages.
package doc2md

import (
	"bytes"
	"fmt"
	"io"
	"path"
	"strings"
	"text/template"

	"golang.org/x/tools/godoc"
	"golang.org/x/tools/godoc/vfs"
)

var (
	// Funcs contains the functions used in the template. Of these only subdir_format might be
	// of interest to callers.
	Funcs = map[string]interface{}{
		"comment_md":    commentMdFunc,
		"base":          path.Base,
		"md":            mdFunc,
		"pre":           preFunc,
		"kebab":         kebabFunc,
		"bitscape":      bitscapeFunc, // Escape [] for bitbucket confusion
		"subdir_format": path.Base,
	}
)

//Config contains config options for doc2md
type Config struct {
	SrcLinkHashFormat string
	SrcLinkFormat     string
	ShowTimestamps    bool
	DeclLinks         bool
	Verbose           bool
	Import            string
	SubPackage        string // If this is a subpackage, this hold the relative import
	GitRef            string // commit, tag, or branch of the repo.
}

func commentMdFunc(comment string) string {
	var buf bytes.Buffer
	toMd(&buf, comment)
	return buf.String()
}

func mdFunc(text string) string {
	text = strings.Replace(text, "*", "\\*", -1)
	text = strings.Replace(text, "_", "\\_", -1)
	return text
}

func preFunc(text string) string {
	return "``` go\n" + text + "\n```"
}

// Removed code line that always substracted 10 from the value of `line`.
// Made format for the source link hash configurable to support source control platforms other than Github.
// Original Source https://github.com/golang/tools/blob/master/godoc/godoc.go#L540
func genSrcPosLinkFunc(srcLinkFormat, srcLinkHashFormat string, config *Config) func(s string, line, low, high int) string {
	return func(s string, line, low, high int) string {
		if srcLinkFormat != "" {
			return fmt.Sprintf(srcLinkFormat, s, line, low, high)
		}

		var buf bytes.Buffer
		template.HTMLEscape(&buf, []byte(s))
		// selection ranges are of form "s=low:high"
		if low < high {
			fmt.Fprintf(&buf, "?s=%d:%d", low, high) // no need for URL escaping
			if line < 1 {
				line = 1
			}
		}
		// line id's in html-printed source are of the
		// form "L%d" (on Github) where %d stands for the line number
		if line > 0 {
			fmt.Fprintf(&buf, srcLinkHashFormat, line) // no need for URL escaping
		}
		return urlForFile(buf.String(), config.Import, config.GitRef, config.SubPackage)
	}
}

func readTemplate(pres *godoc.Presentation, name, data string) (*template.Template, error) {
	t, err := template.New(name).Funcs(pres.FuncMap()).Funcs(Funcs).Parse(data)
	return t, err
}

func kebabFunc(text string) string {
	s := strings.Replace(strings.ToLower(text), " ", "-", -1)
	s = strings.Replace(s, ".", "-", -1)
	s = strings.Replace(s, "\\*", "42", -1)
	return s
}

func bitscapeFunc(text string) string {
	s := strings.Replace(text, "[", "\\[", -1)
	s = strings.Replace(s, "]", "\\]", -1)
	return s
}

// Transform turns your godoc into markdown.The imp (import) path will be used
// for the generated import statement, the same string is also used for generating
// file 'files' links, but then it will be prefixed with 'https://'.
func Transform(out io.Writer, path string, config *Config) error {
	if config.GitRef == "" {
		config.GitRef = "main"
	}

	fs := vfs.NameSpace{}
	corpus := godoc.NewCorpus(fs)
	corpus.Verbose = config.Verbose

	pres := godoc.NewPresentation(corpus)
	pres.TabWidth = 4
	pres.ShowTimestamps = config.ShowTimestamps
	pres.DeclLinks = config.DeclLinks
	pres.URLForSrcPos = genSrcPosLinkFunc(config.SrcLinkFormat, config.SrcLinkHashFormat, config)
	pres.URLForSrc = func(s string) string {
		return urlForFile(s, config.Import, config.GitRef, config.SubPackage)
	}

	tmpl, err := readTemplate(pres, "package.txt", pkgTemplate)
	if err != nil {
		return err
	}

	return write(out, fs, pres, tmpl, path, config.Import)
}

// urlForFile takes path, imp and git ref and sep and creates a link to a file in github or gitlab.
func urlForFile(s, imp, ref, subpkg string) string {
	// make something like
	// https://gitlab.science.ru.nl/cncz/go/-/blob/main/mountinfo/mounted.go
	fileinfo := "/" + path.Base(s)
	importpath := strings.Replace(imp, "go.science.ru.nl", "", 1)
	return "https://gitlab.science.ru.nl/cncz/go/-" + "/blob/" + ref + importpath + fileinfo
}
